# Data Explorer

The .Stat Data Explorer (DE) is a JavaScript application for easy finding, understanding and using data through an efficient well-tuned navigation and search approach, appropriate data previews, and download in standard formats, APIs or share features. 

## Webapp  

This project uses boilerplate dotstatsuite-webapp
  - Multitenant
  - Proxy
  - Config

You can find more about [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-webapp)

## Usage

To start Data-Explorer

First, start a config server:

### Config

```
$ docker run -d --name config --restart always -p 5007:80 siscc/dotstatsuite-config-dev:latest
```

If unlikely you cannot run docker images on your development workstation or you want a custom config,
see how to [run config server](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config) from source.

### Setup

Clone the data-explorer repository.
```
$ yarn
```
Now start `de` server:

```
$ yarn start:srv
...
server started
```

and proxy
```
$ yarn start:proxy
```

Launch your preferred browser on http://localhost:7000

If it does not work, check log messages
