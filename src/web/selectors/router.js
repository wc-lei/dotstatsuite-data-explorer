import { createSelector } from 'reselect';
import * as R from 'ramda';
import { i18n, locales, getDatasource } from '../lib/settings';
import { fromSearchToState } from '../utils/router';

//------------------------------------------------------------------------------------------------#0
const getRouter = R.prop('router');

//------------------------------------------------------------------------------------------------#1
export const getLocation = createSelector(getRouter, R.propOr({}, 'location'));

//------------------------------------------------------------------------------------------------#2
export const getLocationState = createSelector(
  getLocation,
  R.pipe(R.prop('state'), R.defaultTo(fromSearchToState(window.location.search))),
);

export const getPathname = createSelector(getLocation, R.propOr('/', 'pathname'));

//------------------------------------------------------------------------------------------------#3
export const getTerm = createSelector(getLocationState, R.propOr('', 'term'));
export const getStart = createSelector(getLocationState, R.prop('start'));
export const getLocale = createSelector(getLocationState, R.propOr(i18n.localeId, 'locale'));
export const getFacet = createSelector(getLocationState, R.prop('facet'));
export const getConstraints = createSelector(getLocationState, R.propOr({}, 'constraints'));
export const getDataflow = createSelector(getLocationState, R.propOr({}, 'dataflow'));
export const getFilter = createSelector(getLocationState, R.prop('filter'));
export const getDataquery = createSelector(getLocationState, R.propOr('', 'dataquery'));
export const getHasAccessibility = createSelector(getLocationState, R.prop('hasAccessibility'));
export const getHasDataAvailability = createSelector(
  getLocationState,
  R.propOr(true, 'hasDataAvailability'),
);
export const getViewer = createSelector(getLocationState, R.propOr('table', 'viewer'));
export const getMap = createSelector(getLocationState, R.prop('map'));
export const getFrequency = createSelector(getLocationState, R.prop('frequency'));
export const getPeriod = createSelector(getLocationState, R.prop('period'));
export const getTableLayout = createSelector(getLocationState, R.prop('layout'));
export const getDisplay = createSelector(getLocationState, R.propOr('label', 'display'));
export const getTimeDimensionOrders = createSelector(getLocationState, R.propOr({}, 'time'));

//------------------------------------------------------------------------------------------------#4
export const getHasLastNObservations = createSelector(
  getDataflow,
  R.pipe(R.prop('datasourceId'), getDatasource, R.propOr(false, 'hasLastNObservations')),
);

//------------------------------------------------------------------------------------------------#5
export const getLastNObservations = createSelector(
  getLocationState,
  getHasLastNObservations,
  (state, hasLastNObservations) =>
    hasLastNObservations ? R.prop('lastNObservations', state) : null,
);

//--------------------------------------------------------------------------------------------------
export const getIsRtl = createSelector(getLocale, localeId =>
  R.pipe(R.prop(localeId), R.prop('isRtl'))(locales),
);
