import bs62 from 'bs62';
import * as search from '../search';

jest.mock('../../lib/settings', () => ({
  search: { pinnedFacetIds: ['pinned'], excludedFacetIds: ['excluded'] },
  i18n: { localeId: 'en' },
}));

const state = search => ({ search: { ...search } });

describe('search selectors', () => {
  it('should handle no facet', () => {
    expect(search.getResultsFacets(state())).toEqual([]);
  });

  it('should handle pinnedFacetIds', () => {
    const facets = [
      { id: bs62.encode('country'), values: [{ id: '1' }] },
      { id: bs62.encode('pinned'), values: [{ id: '2' }] },
    ];
    expect(search.getResultsFacets(state({ facets }))).toEqual([
      { id: bs62.encode('pinned'), isPinned: true, values: [{ id: '2' }] },
      { id: bs62.encode('country'), values: [{ id: '1' }] },
    ]);
  });

  it('should handle excludedFacetIds', () => {
    const facets = [
      { id: bs62.encode('excluded'), values: [{ id: '1' }] },
      { id: bs62.encode('home'), values: [{ id: '2' }] },
    ];
    expect(search.getResultsFacets(state({ facets }))).toEqual([facets[1]]);
  });
});
