import * as search from '../search';

jest.mock('../../lib/settings', () => ({
  search: { pinnedFacetIds: ['datasourceId'] },
  i18n: { localeId: 'en' },
}));

const state = search => ({ search: { ...search } });

describe('search selectors', () => {
  it('should handle datasource id pinned', () => {
    const facets = [{ id: 'datasourceId', values: [{ id: '1' }] }];
    expect(search.getResultsFacets(state({ facets }))).toEqual([
      { id: 'datasourceId', isPinned: true, values: [{ id: '1' }] },
    ]);
  });
});
