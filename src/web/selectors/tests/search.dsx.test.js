import bs62 from 'bs62';
import * as search from '../search';

jest.mock('../../lib/settings', () => ({
  search: { excludedFacetIds: ['datasourceId'] },
  i18n: { localeId: 'en' },
}));

const state = search => ({ search: { ...search } });

describe('search selectors', () => {
  it('should handle datasource id exluded', () => {
    const facets = [{ id: 'datasourceId', values: [{ id: '1' }] }];
    expect(search.getResultsFacets(state({ facets }))).toEqual([]);
  });
});
