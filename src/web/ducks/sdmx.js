import * as R from 'ramda';
import { updateDataquery, getDataquery } from '@sis-cc/dotstatsuite-sdmxjs';
import { getFilter, getDataquery as getRouterDataquery, getFrequency } from '../selectors/router';
import {
  getContentConstrainedDimensions,
  getContentConstraints,
  getDimensions,
  getFrequencyArtefact,
  getFrequencyArtefactContentConstrained,
  getPeriod,
} from '../selectors/sdmx';
import { getSelectedIdsIndexed, setSelectedDimensionsValues } from '../lib/sdmx';
import { getSdmxPeriod, changeFrequency, getFrequencies } from '../lib/sdmx/frequency';
import { START_PERIOD, END_PERIOD, LASTN } from '../utils/used-filter';

//---------------------------------------------------------------------------------------------model
export const model = () => ({
  dimensions: [],
  data: undefined,
  layout: {},
  frequencyArtefact: {},
  externalResources: [],
  availableFrequencies: { ids: [], labels: {} },
  hasContentConstraints: false,
  name: undefined,
});

//-----------------------------------------------------------------------------------------constants
export const HANDLE_STRUCTURE = '@@sdmx/HANDLE_STRUCTURE';
export const REQUEST_STRUCTURE = '@@sdmx/REQUEST_STRUCTURE';
export const PARSE_STRUCTURE = '@@sdmx/PARSE_STRUCTURE';
export const HANDLE_DATA = '@@sdmx/HANDLE_DATA';
export const REQUEST_DATA = '@@sdmx/REQUEST_DATA';
export const PARSE_DATA = '@@sdmx/PARSE_DATA';
export const FLUSH_DATA = '@@sdmx/FLUSH_DATA';
export const RESET_SDMX = '@@sdmx/RESET_SDMX';
export const REQUEST_DATAFILE = '@@sdmx/REQUEST_DATAFILE';
export const CHANGE_DATAFLOW = '@@sdmx/CHANGE_DATAFLOW';
export const RESET_DATAFLOW = '@@sdmx/RESET_DATAFLOW';
export const CHANGE_FILTER = '@@sdmx/CHANGE_FILTER';
export const CHANGE_DATAQUERY = '@@sdmx/CHANGE_DATAQUERY';
export const APPLY_DATA_AVAILABILITY = '@@sdmx/APPLY_DATA_AVAILABILITY';
export const CHANGE_FREQUENCY_PERIOD = '@@sdmx/CHANGE_FREQUENCY_PERIOD';
export const CHANGE_LAST_N_OBS = '@@sdmx/CHANGE_LAST_N_OBS';
export const RESET_SPECIAL_FILTERS = '@@sdmx/RESET_SPECIAL_FILTERS';

//------------------------------------------------------------------------------------------creators
export const requestData = ({ shouldRequestStructure } = {}) => ({
  type: REQUEST_DATA,
  shouldRequestStructure,
});

export const requestDataFile = ({ isDownloadAllData, dataflow }) => ({
  type: REQUEST_DATAFILE,
  payload: { isDownloadAllData, dataflow },
});

export const resetSdmx = () => ({ type: RESET_SDMX });

export const changeDataflow = dataflow => ({
  type: CHANGE_DATAFLOW,
  pushHistory: { pathname: '/vis', payload: { dataflow } },
});

export const resetDataflow = () => dispatch => {
  dispatch(resetSdmx());
  dispatch({
    type: RESET_DATAFLOW,
    pushHistory: {
      pathname: '/',
      payload: {
        dataflow: null,
        filter: null,
        dataquery: null,
        hasDataAvailability: null,
        viewer: null,
        // period: [undefined, undefined] is used to force no period, null means that default can be used
        period: null,
        frequency: null,
        lastNObservations: null,
        layout: null,
        time: null,
      },
    },
  });
};

export const changeLastNObservations = lastNObservations => ({
  type: CHANGE_LAST_N_OBS,
  pushHistory: { pathname: '/vis', payload: { lastNObservations } },
  request: 'getData',
});

export const resetFilters = () => (dispatch, getState) => {
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: {
      pathname: '/vis',
      payload: {
        dataquery: updateDataquery(
          getContentConstrainedDimensions(getState()),
          getRouterDataquery(getState()),
          null,
          null,
        ),
        period: [undefined, undefined],
        frequency: getFrequency(getState()),
        lastNObservations: null,
      },
    },
    request: 'getData',
  });
};

export const resetSpecialFilters = (_, id) => (dispatch, getState) => {
  // special is lastNObservations
  if (R.equals(LASTN, id)) return dispatch(changeLastNObservations());

  // special is start OR end (period)
  if (R.either(R.equals(START_PERIOD), R.equals(END_PERIOD))(id)) {
    const payloadPeriod = period => {
      if (R.equals(START_PERIOD, id)) return [undefined, R.last(period)];
      if (R.equals(END_PERIOD, id)) return [R.head(period), undefined];
    };
    return dispatch({
      type: CHANGE_FREQUENCY_PERIOD,
      request: 'getData',
      pushHistory: { pathname: '/vis', payload: { period: payloadPeriod(getPeriod(getState())) } },
    });
  }

  // reset all special filters (period and lastNObservations)
  dispatch({
    type: RESET_SPECIAL_FILTERS,
    pushHistory: {
      pathname: '/vis',
      payload: { period: [undefined, undefined], lastNObservations: null },
    },
    request: 'getData',
  });
};

export const changeFilter = filterId => (dispatch, getState) => {
  const prevFilterId = getFilter(getState());
  const filter = R.ifElse(R.equals(prevFilterId), R.always(null), R.identity)(filterId);
  dispatch({ type: CHANGE_FILTER, pushHistory: { pathname: '/vis', payload: { filter } } });
};

export const changeDataquery = (filterId, valueId) => (dispatch, getState) => {
  const dataquery = updateDataquery(
    getContentConstrainedDimensions(getState()),
    getRouterDataquery(getState()),
    filterId,
    valueId,
  );
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: { pathname: '/vis', payload: { dataquery } },
    request: 'getData',
  });
};

export const applyDataAvailability = hasDataAvailability => (dispatch, getState) => {
  dispatch({
    type: APPLY_DATA_AVAILABILITY,
    pushHistory: { pathname: '/vis', payload: { hasDataAvailability } },
  });

  const contentConstraints = getContentConstraints(getState());
  if (R.isNil(contentConstraints)) return;

  const currentDataquery = getRouterDataquery(getState());
  const dimensions = hasDataAvailability
    ? getDimensions(getState())
    : getContentConstrainedDimensions(getState());
  const dimensionsWithSelectedValues = setSelectedDimensionsValues(currentDataquery, dimensions);
  const frequencyArtefact = hasDataAvailability
    ? getFrequencyArtefact(getState())
    : getFrequencyArtefactContentConstrained(getState());
  const frequencyId = R.prop('id')(frequencyArtefact);
  const availableFrequencies = getFrequencies(frequencyArtefact);
  const frequency = changeFrequency(getFrequency(getState()))(availableFrequencies);
  const selection = R.assoc(
    frequencyId,
    frequency,
    getSelectedIdsIndexed(dimensionsWithSelectedValues),
  );
  const dataquery = getDataquery(dimensions, selection);

  if (R.equals(currentDataquery, dataquery)) return;

  dispatch({ type: CHANGE_FREQUENCY_PERIOD, payload: { frequency } });
  dispatch({
    type: CHANGE_DATAQUERY,
    pushHistory: { pathname: '/vis', payload: { dataquery } },
    request: 'getData',
  });
};

export const changeFrequencyPeriod = ({ valueId, dates } = {}) => (dispatch, getState) => {
  const period = R.or(R.isEmpty(dates), R.isNil(dates)) ? [undefined, undefined] : dates;
  const filterId = R.prop('id')(getFrequencyArtefactContentConstrained(getState()));
  const action = {
    type: CHANGE_FREQUENCY_PERIOD,
    pushHistory: {
      pathname: '/vis',
      payload: { frequency: valueId, period: R.map(getSdmxPeriod(valueId))(period) },
    },
  };

  if (R.not(R.isNil(filterId))) {
    dispatch(action);
    dispatch(changeDataquery(filterId, valueId));
    return;
  }

  dispatch(R.assoc('request', 'getData', action));
};

//-------------------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case FLUSH_DATA:
      return R.set(R.lensProp('data'), undefined, state);
    case HANDLE_STRUCTURE:
      return R.pipe(
        R.set(
          R.lensProp('hasContentConstraints'),
          R.path(['structure', 'hasContentConstraints'], action),
        ),
        R.set(R.lensProp('externalResources'), R.path(['structure', 'externalResources'], action)),
        R.set(R.lensProp('frequencyArtefact'), R.path(['structure', 'frequencyArtefact'], action)),
        R.set(R.lensProp('dimensions'), R.path(['structure', 'dimensions'], action)),
        R.set(R.lensProp('timePeriod'), R.path(['structure', 'timePeriod'], action)),
        R.set(R.lensProp('name'), R.path(['structure', 'title'], action)),
        R.set(
          R.lensProp('contentConstraints'),
          R.path(['structure', 'contentConstraints'], action),
        ),
        R.set(
          R.lensProp('timePeriodBoundaries'),
          R.path(['structure', 'timePeriodBoundaries'], action),
        ),
        R.set(
          R.lensProp('timePeriodIncludingBoundaries'),
          R.path(['structure', 'timePeriodIncludingBoundaries'], action),
        ),
        R.dissoc('range'),
      )(state);
    case HANDLE_DATA:
      return R.pipe(
        R.set(R.lensProp('data'), R.prop('data', action)),
        R.set(R.lensProp('range'), R.prop('range', action)),
      )(state);
    case RESET_SDMX:
      return { ...state, ...model() };
    default:
      return state;
  }
};
