import * as R from 'ramda';
import { setLocale } from '../i18n';

//------------------------------------------------------------------------------constants
export const LOG_ERROR = 'LOG_ERROR';

//----------------------------------------------------------------------------------model
export const model = () => ({
  logs: [],
  pending: {},
});

//--------------------------------------------------------------------------------actions
export const PUSH_LOG = '@@app/PUSH_LOG';
export const SET_PENDING = '@@app/SET_PENDING';
export const CHANGE_HAS_ACCESSIBILITY = '@@app/CHANGE_HAS_ACCESSIBILITY';
export const CHANGE_LOCALE = '@@app/CHANGE_LOCALE';

//-------------------------------------------------------------------------------creators
export const pushLog = log => ({ type: PUSH_LOG, payload: { log } });

export const setPending = (id, is) => ({ type: SET_PENDING, payload: { id, is } });

export const changeHasAccessibility = hasAccessibility => ({
  type: CHANGE_HAS_ACCESSIBILITY,
  pushHistory: { payload: { hasAccessibility } },
});

export const changeLocale = locale => dispatch => {
  setLocale(locale);
  dispatch({
    type: CHANGE_LOCALE,
    pushHistory: { payload: { locale, term: '', start: 0, constraints: null, facet: null } },
  });
};

//--------------------------------------------------------------------------------reducer
export default (state = model(), action = {}) => {
  switch (action.type) {
    case PUSH_LOG:
      return R.over(R.lensProp('logs'), R.prepend(action.payload.log), state);
    case SET_PENDING:
      return R.assocPath(['pending', action.payload.id], action.payload.is, state);
    default:
      return state;
  }
};
