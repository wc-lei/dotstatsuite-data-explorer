const model = () => ({
  authenticating: true,
  me: undefined,
  tokenId: undefined,
});

const USER_SIGNED_IN = 'USER_SIGNED_IN';

export const PUSH_TOKEN = 'PUSH_TOKEN';

const DELETE_TOKEN = 'DELETE_TOKEN';

export const AUTHENTICATED = 'AUTHENTICATED';

export const userSignIn = ({ name, email, sub: id }) => ({
  type: USER_SIGNED_IN,
  user: { name, email, id },
});

export const pushToken = tokenId => ({
  type: PUSH_TOKEN,
  payload: { tokenId },
});

export const deleteToken = () => ({ type: DELETE_TOKEN });

export default (state = model(), action = {}) => {
  switch (action.type) {
    case AUTHENTICATED:
      return { ...state, authenticating: false };
    case USER_SIGNED_IN:
      return { ...state, me: action.user };
    case PUSH_TOKEN:
      return { ...state, tokenId: action.payload.tokenId };
    case DELETE_TOKEN:
      return { ...state, tokenId: undefined };
    default:
      return state;
  }
};
