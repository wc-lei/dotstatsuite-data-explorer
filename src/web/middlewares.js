import * as R from 'ramda';
import ReactGA from 'react-ga';
import { push } from 'connected-react-router';
import { DOWNLOAD_EXCEL_SUCCESS, SHARE_SUCCESS, DOWNLOAD_PNG_CHART_SUCCESS } from './ducks/vis';
import { requestData, REQUEST_DATAFILE, HANDLE_STRUCTURE } from './ducks/sdmx';
import { requestSearch } from './ducks/search';
import { fromStateToSearch } from './utils/router';
import { getLocationState, getDataflow, getViewer, getPathname } from './selectors/router';
import { getDataflowName } from './selectors/sdmx';
import { getVisDataDimensions } from './selectors';
import { joinDataflowIds } from './utils/analytics';
import { compactLayout } from './lib/layout';

const isDev = process.env.NODE_ENV === 'development';

export const historyMiddleware = store => next => action => {
  if (R.not(R.has('pushHistory', action))) return next(action);

  const prevState = getLocationState(store.getState());
  const pathname = R.defaultTo(getPathname(store.getState()), action.pushHistory.pathname);
  const payload = action.pushHistory.payload;

  const state = R.ifElse(
    R.isNil,
    // reset if no payload with invariant id present (see app duck)
    R.always(R.pick(['locale', 'hasAccessibility', 'tenant'], prevState)),
    R.mergeRight(prevState),
  )(payload);

  // fromStateToSearch is an evolution centered on url
  // the following evolution relies on selectors (more generally on things in state)
  const nextState = R.evolve({
    layout: compactLayout(getVisDataDimensions()(store.getState())),
  })(state);

  store.dispatch(push({ pathname, state, search: fromStateToSearch(nextState) }));

  // eslint-disable-next-line no-console
  if (isDev) console.info(`[MDL] history ${action.type} -> ${pathname}`);

  return next(action);
};

export const requestMiddleware = store => next => action => {
  const request = R.prop('request', action);

  if (R.isNil(request)) return next(action);

  switch (request) {
    case 'getSearch':
      store.dispatch(requestSearch());
      break;
    case 'getData':
      store.dispatch(requestData());
      break;
    case 'getStructure':
      store.dispatch(requestData({ shouldRequestStructure: true }));
      break;
    default:
      // eslint-disable-next-line no-console
      if (isDev) console.log(`[MDL] request unknown request ${request}`);
      return next(action);
  }

  // eslint-disable-next-line no-console
  if (isDev) console.info(`[MDL] request ${action.type} -> ${request}`);

  return next(action);
};

const analyticsAction = new Set([
  REQUEST_DATAFILE,
  DOWNLOAD_EXCEL_SUCCESS,
  SHARE_SUCCESS,
  HANDLE_STRUCTURE,
  DOWNLOAD_PNG_CHART_SUCCESS,
]);

export const analyticsMiddleware = ({ getState }) => next => action => {
  if (analyticsAction.has(action.type)) {
    const future = next(action);
    // eslint-disable-next-line no-console
    if (isDev) console.info(`analyticsMiddleware: ${action.type}`);
    const { payload, type } = action;
    const dataflowIdentifier = R.join(' ')([
      getDataflowName(getState()),
      joinDataflowIds(getDataflow(getState())),
    ]);

    switch (type) {
      case REQUEST_DATAFILE:
        return ReactGA.event({
          category: 'DOWNLOAD',
          action: R.prop('isDownloadAllData')(payload) ? 'CSV_FULL' : 'CSV',
          label: R.ifElse(
            R.hasPath(['dataflow', 'name']),
            R.converge(R.pipe(R.prepend, R.join(' ')), [
              R.path(['dataflow', 'name']),
              R.pipe(R.prop('dataflow'), joinDataflowIds, R.of),
            ]),
            R.always(dataflowIdentifier),
          )(payload),
        });
      case DOWNLOAD_EXCEL_SUCCESS:
        return ReactGA.event({
          category: 'DOWNLOAD',
          action: 'EXCEL',
          label: dataflowIdentifier,
        });
      case DOWNLOAD_PNG_CHART_SUCCESS:
        return ReactGA.event({
          category: 'DOWNLOAD',
          action: 'PNG',
          label: dataflowIdentifier,
        });
      case SHARE_SUCCESS:
        return ReactGA.event({
          category: 'SHARE',
          action: `SHARED_${R.toUpper(getViewer(getState()))}`,
          label: dataflowIdentifier,
        });
      case HANDLE_STRUCTURE:
        return ReactGA.event({
          category: 'DATAFLOW',
          action: 'VIEWED_DATAFLOW',
          label: dataflowIdentifier,
          nonInteraction: true,
        });
    }
    return future;
  }
  return next(action);
};
