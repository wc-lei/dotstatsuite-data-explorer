import * as R from 'ramda';
import { select, put, call, takeLatest } from 'redux-saga/effects';
import axios from 'axios';
import { getVisChoroMap } from '../selectors';
import { getMap as getRouterMap } from '../selectors/router';
import * as Settings from '../lib/settings';
import { CHANGE_VIEWER } from '../ducks/vis';
import { REQUEST_STRUCTURE } from '../ducks/sdmx';
import { loadingMap, loadMapError, loadMapSuccess } from '../ducks/vis';

function* loadMap() {
  const routerMap = yield select(getRouterMap);
  if (R.anyPass([R.isNil, R.isEmpty])(routerMap)) {
    return;
  }
  const choroMap = yield select(getVisChoroMap);
  const { mapId, levelId } = R.pick(['levelId', 'mapId'], routerMap);
  if (
    !R.isNil(choroMap) &&
    R.prop('id', choroMap) === mapId &&
    R.prop('areaSelection', choroMap) === levelId
  ) {
    return;
  }
  yield put(loadingMap());
  try {
    if (R.isNil(mapId) || R.isNil(levelId)) {
      throw new Error('missing map entries');
    }
    const map = Settings.getMap(mapId);
    if (R.isNil(map)) {
      throw new Error('inexistant map entry');
    }
    const response = yield call(axios.get, map.path);
    const topology = response.data;
    const completeMap = R.pipe(
      R.assoc('topology', topology),
      R.assoc('areaSelection', levelId),
    )(map);
    yield put(loadMapSuccess(completeMap));
  } catch (error) {
    yield put(loadMapError(error));
  }
}

export function* mapSaga() {
  yield takeLatest([CHANGE_VIEWER, REQUEST_STRUCTURE], loadMap);
}
