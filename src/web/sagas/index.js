import { all } from 'redux-saga/effects';
import { watchRequestStructure, watchParseStructure } from './sdmx/structure';
import { watchRequestData, watchParseData, watchRequestDataFile } from './sdmx/data';
import { mapSaga } from './map';

export default function* rootSaga() {
  yield all([
    watchRequestStructure(),
    watchParseStructure(),
    watchRequestData(),
    watchRequestDataFile(),
    watchParseData(),
    mapSaga(),
  ]);
}
