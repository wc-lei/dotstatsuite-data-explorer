import axios from 'axios';
import FileSaver from 'file-saver';
import * as R from 'ramda';
import { select, put, call, takeLatest, take, cancel } from 'redux-saga/effects';
import { parseDataRange } from '@sis-cc/dotstatsuite-sdmxjs';
import { rules } from '@sis-cc/dotstatsuite-components';
import {
  getDataRequestArgs,
  getDataFileRequestArgs,
  getFullDataflowFileRequestArgs,
  getContentConstrainedDimensions,
} from '../../selectors/sdmx';
import {
  HANDLE_DATA,
  REQUEST_DATA,
  REQUEST_DATAFILE,
  PARSE_DATA,
  FLUSH_DATA,
  REQUEST_STRUCTURE,
  HANDLE_STRUCTURE,
} from '../../ducks/sdmx';
import { setPending, pushLog, LOG_ERROR } from '../../ducks/app';
import { locales } from '../../lib/settings';
import { getTokenIdUtil } from '../token';
import { getLocale, getDisplay } from '../../selectors/router';

const isDev = process.env.NODE_ENV === 'development';

const downloadFile = ({ response, filename }) => {
  const blob = new Blob([R.prop('data')(response)], {
    type: R.pathOr('text/csv', ['headers', 'content-type'])(response),
  });
  FileSaver.saveAs(blob, `${filename}.csv`);
};

function* requestDataFile(action) {
  const { isDownloadAllData, dataflow } = action.payload;
  const display = yield select(getDisplay);
  const argsSelector = R.when(
    R.always(!!isDownloadAllData),
    R.always(getFullDataflowFileRequestArgs(dataflow)),
  )(getDataFileRequestArgs(display));
  const { url, headers, params, filename } = yield select(argsSelector);
  const pendingId = R.ifElse(
    R.has('id'),
    R.pipe(R.prop('id'), id => `getDataFile/${id}`),
    R.always('requestingDataFile'),
  )(dataflow);
  try {
    yield put(setPending(pendingId, true));
    const tokenId = yield call(getTokenIdUtil);
    const authHeaders = R.when(
      R.always(!R.isNil(tokenId)),
      R.assoc('Authorization', `Bearer ${tokenId}`),
    )(headers);
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const response = yield call(axios.get, url, { headers: authHeaders, params });
    yield call(downloadFile, { response, filename });
    yield put(setPending(pendingId, false));
  } catch (error) {
    const log = error.response
      ? { errorCode: error.response.data.errorCode, statusCode: error.response.status }
      : { error };

    yield put(setPending(pendingId, false));
    yield put(pushLog({ type: LOG_ERROR, log }));
  }
}

function* requestData({ shouldRequestStructure }) {
  const pendingId = 'getData';

  try {
    yield put({ type: FLUSH_DATA });
    if (shouldRequestStructure) {
      yield put({ type: REQUEST_STRUCTURE });
      yield take(HANDLE_STRUCTURE);
    }
    const dimensions = yield select(getContentConstrainedDimensions);
    if (R.all(R.isEmpty)(dimensions)) {
      console.log('Saga Data: Dimensions are completely empty, cancel requestData'); // eslint-disable-line no-console
      yield cancel();
    }
    const { url, headers, params } = yield select(getDataRequestArgs);
    yield put(setPending(pendingId, true));
    const tokenId = yield call(getTokenIdUtil);
    const authHeaders = R.when(
      R.always(!R.isNil(tokenId)),
      R.assoc('Authorization', `Bearer ${tokenId}`),
    )(headers);
    if (isDev) console.info(`request: ${pendingId}`); // eslint-disable-line no-console
    const response = yield call(axios.get, url, { headers: authHeaders, params });
    yield put(setPending(pendingId, false));
    yield put({ type: PARSE_DATA, data: response });
  } catch (error) {
    yield put(setPending(pendingId, false));

    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data); // eslint-disable-line no-console
      console.log(error.response.status); // eslint-disable-line no-console
      console.log(error.response.headers); // eslint-disable-line no-console
      yield put(pushLog({ type: 'response', log: { ...error.response } }));
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request); // eslint-disable-line no-console
      yield put(pushLog({ type: 'request', log: error }));
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message); // eslint-disable-line no-console
      yield put(pushLog({ type: 'error', log: error }));
    }
  }
}

function* parseData({ data }) {
  const locale = yield select(getLocale);
  try {
    yield put({
      type: HANDLE_DATA,
      data: R.prop(
        'data',
        rules.v8Transformer(data.data, { timeFormat: R.path([locale, 'timeFormat'])(locales) }),
      ),
      range: parseDataRange(data),
    });
  } catch (error) {
    yield put(pushLog({ type: 'parse', log: error }));
  }
}

export function* watchRequestData() {
  yield takeLatest(REQUEST_DATA, requestData);
}

export function* watchRequestDataFile() {
  yield takeLatest(REQUEST_DATAFILE, requestDataFile);
}

export function* watchParseData() {
  yield takeLatest(PARSE_DATA, parseData);
}
