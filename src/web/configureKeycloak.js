import * as R from 'ramda';
import keycloakAdapter from 'keycloak-js';
import { userSignIn, pushToken, deleteToken, AUTHENTICATED } from './ducks/user';

const isDev = process.env.NODE_ENV === 'development';

export default ({ dispatch }) => {
  const { keycloak: keycloakOIDC } = window.CONFIG;
  const keycloak = keycloakAdapter(keycloakOIDC);

  return {
    initConfig: { onLoad: 'check-sso', promiseType: 'native', pkceMethod: 'S256' },
    keycloak,
    onEvent: (event, error) => {
      if (event === 'onAuthSuccess') keycloak.loadUserInfo().then(ui => dispatch(userSignIn(ui)));
      if (event === 'onReady') dispatch({ type: AUTHENTICATED });
      if (event === 'onTokenExpired') dispatch(deleteToken());

      if (isDev) {
        console.log('[KEYCLOAK] onEvent', event); // eslint-disable-line no-console
        if (error) console.log('[KEYCLOAK] error', error); // eslint-disable-line no-console
      }
    },
    onTokens: tokens => {
      dispatch(pushToken(tokens.token));
      if (isDev) {
        console.log('[KEYCLOAK] onTokens'); // eslint-disable-line no-console
        console.table(R.toPairs(tokens)); // eslint-disable-line no-console
      }
    },
  };
};
