import { cleanDeadBranches } from '../clean-dead-branches';

describe('DE - utils - makeTree', () => {
  test('should empty array no items', () => {
    expect(cleanDeadBranches([])).toEqual([]);
  });
  test('flat list with one branch', () => {
    const items = [
      { id: 1, label: 'abc' },
      { id: 20, parentId: 1, label: 'abd2' },
      { id: 30, parentId: 1, label: 'abd3' },
      { id: 200, parentId: 20, label: 'bcd2', hasData: true },
    ];
    const expected = [
      { hasData: true, id: 200, label: "bcd2", parentId: 20 },
      { hasChild: true, id: 20, label: "abd2", parentId: 1 },
      { hasChild: true, id: 1, label: "abc" }
    ];
    expect(cleanDeadBranches(items)).toEqual(expected);
  });
  test('flat list with two branches', () => {
    const items = [
      { id: 220, parentId: 2, label: '22', hasData: true },
      { id: 1, label: 'abc' },
      { id: 2, label: 'abc2' },
      { id: 20, parentId: 1, label: 'abd2' },
      { id: 30, parentId: 1, label: 'abd3' },
      { id: 200, parentId: 20, label: 'bcd2', hasData: true },
    ];
    const expected = [
      { hasData: true, id: 220, label: "22", parentId: 2 },
      { hasChild: true, id: 2, label: "abc2" },
      { hasData: true, id: 200, label: "bcd2", parentId: 20 },
      { hasChild: true, id: 20, label: "abd2", parentId: 1 },
      { hasChild: true, id: 1, label: "abc" }
    ];
    expect(cleanDeadBranches(items)).toEqual(expected);
  });
  test('flat list with data only in the middle', () => {
    const items = [
      { id: 1, label: 'abc' },
      { id: 2, label: 'abc2' },
      { id: 20, parentId: 1, label: 'abd2' },
      { id: 200, parentId: 20, label: 'bcd2', hasData: true },
      { id: 2000, parentId: 200, label: 'bcd3'},
      { id: 20000, parentId: 2000, label: 'bcd4'},
    ];
    const expected = [
      { hasData: true, id: 200, label: "bcd2", parentId: 20 },
      { hasChild: true, id: 20, label: "abd2", parentId: 1 },
      { hasChild: true, id: 1, label: "abc" }
    ];
    expect(cleanDeadBranches(items)).toEqual(expected);
  });
  test('no data', () => {
    const items = [
      { id: 1, label: 'abc' },
      { id: 2, label: 'abc2' },
      { id: 20, parentId: 1, label: 'abd2' },
      { id: 200, parentId: 20, label: 'bcd2'},
      { id: 2000, parentId: 200, label: 'bcd3'},
      { id: 20000, parentId: 2000, label: 'bcd4'},
    ];
    expect(cleanDeadBranches(items)).toEqual([]);
  });
});
