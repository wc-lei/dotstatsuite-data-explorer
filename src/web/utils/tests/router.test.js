import * as R from 'ramda';
import { fromStateToSearch, fromSearchToState } from '../router';

describe('utils of router ', () => {
  const state = {
    tenant: 'dev',
    facet: 'eco',
    constraints: {
      '1bee525958b5c71ff731b7badbaa861f': { facetId: 'cou', constraintId: 'fra' },
      ae3b7693c6547dceacf02b9f84e84640: { facetId: 'ind', constraintId: 'yma' },
    },
    dataflow: {
      datasourceId: '1',
      dataflowId: '2',
      agencyId: 'rp',
      version: '1.0',
      prop: 3,
    },
  };

  const search =
    'tenant=dev&facet=eco&constraints[0]=cou%2Cfra&constraints[1]=ind%2Cyma&dataflow[datasourceId]=1&dataflow[dataflowId]=2&dataflow[agencyId]=rp&dataflow[version]=1.0';

  it('should process a state into a search for browser history', () => {
    expect(fromStateToSearch(state)).toEqual(search);
  });

  it('should process a search for browser history into a state', () => {
    expect(fromSearchToState(search)).toEqual({
      ...state,
      dataflow: R.omit(['prop'], state.dataflow),
    });
  });
});
