import * as R from 'ramda';
import qs from 'qs';
import md5 from 'md5';

const fromStateDataflow = R.pipe(
  R.defaultTo({}),
  R.pick(['datasourceId', 'dataflowId', 'agencyId', 'version']),
);
const fromStateConstraints = R.pipe(R.values, R.mapObjIndexed(R.values));
const fromStatePeriod = R.ifElse(
  R.isNil,
  R.identity,
  R.pipe(R.reject(R.isNil), R.ifElse(R.isEmpty, R.always(null), R.identity)),
);

export const fromStateToSearch = state => {
  const qsOptions = { encodeValuesOnly: true, skipNulls: true, arrayFormat: 'comma' };
  const preparedState = R.pipe(
    R.evolve({
      constraints: fromStateConstraints,
      dataflow: fromStateDataflow,
      period: fromStatePeriod,
    }),
    R.reject(R.isEmpty),
  )(state);
  return qs.stringify(preparedState, qsOptions);
};

const toStateBoolean = R.equals('true');

const toStateConstraints = R.reduce(
  (memo, [facetId, constraintId]) =>
    R.assoc(md5(`${facetId}${constraintId}`), { facetId, constraintId }, memo),
  {},
);

export const fromSearchToState = search => {
  const qsOptions = { ignoreQueryPrefix: true, comma: true };
  const state = qs.parse(search, qsOptions);
  return R.evolve({
    constraints: toStateConstraints,
    hasAccessibility: toStateBoolean,
    hasDataAvailability: toStateBoolean,
  })(state);
};

export const getVisUrl = (state, dataflow = {}) =>
  `/vis?${fromStateToSearch({ ...state, dataflow: fromStateDataflow(dataflow) })}`;
