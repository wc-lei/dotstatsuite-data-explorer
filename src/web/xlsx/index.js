import XlsxPopulate from 'xlsx-populate';
import * as R from 'ramda';
import { SHEET1, SHEET2, SHEETS_INDEX } from './constants';
import { registerCell, registerSequenceInRow, registerSequenceInColumn } from './register';
import { getPosition, formatSection } from './utils';
import { getSectionStyles, getHeaderStyles, getRowStyles, getRtlStyles } from './styles';

export const createExcelWorkbook = ({ footerProps, headerProps, tableProps, theme }) => {
  const { direction } = theme;
  const isRtl = R.equals('rtl', direction);
  return XlsxPopulate.fromBlankAsync().then(workbook => {
    workbook.addSheet(SHEET1, 0);
    workbook.addSheet(SHEET2, 1);
    workbook.sheet(0).rightToLeft(isRtl);
    workbook.sheet(1).rightToLeft(isRtl);

    const { cells, headerData, sectionsData } = tableProps;

    const rtlStyles = getRtlStyles(isRtl);
    const headerStyles = getHeaderStyles(theme, rtlStyles);
    const sectionStyles = getSectionStyles(theme, rtlStyles);
    const rowStyles = getRowStyles(theme, rtlStyles);

    const tableLengthInRow = R.length(headerData);
    const rowDimensions = R.pipe(
      R.pathOr([], [0, 1, 0, 'data']),
      R.pluck('dimension'),
    )(sectionsData);
    const rowDimensionsLength = R.length(rowDimensions);
    const tableFullLength = tableLengthInRow + rowDimensionsLength;
    const startTablePosition = 2; // 1 = A, 2 = B
    const spaceLength = 1;
    let rowIndex = 1;
    registerCell(
      workbook,
      getPosition(rowIndex, startTablePosition),
      R.propOr({}, 'title', headerProps),
      {
        bold: true,
        ...rtlStyles,
      },
    );
    rowIndex++;

    const subtitle = R.propOr([], 'subtitle', headerProps);
    registerSequenceInRow(workbook, rowIndex, startTablePosition, subtitle, rtlStyles);
    rowIndex = R.length(subtitle) + rowIndex;

    registerCell(
      workbook,
      getPosition(rowIndex, startTablePosition),
      R.propOr({}, 'uprs', headerProps),
      rtlStyles,
    );
    rowIndex += 2;
    const beginRowIndex = rowIndex;
    /*
      header
      | dim1 | col1 val | ... | coln val |
                    ...
      | dimn | col1 val | ... | coln val |
    */
    const transposedHeaderData = R.pipe(R.pluck('data'), R.transpose)(headerData);
    R.forEach(headerRow => {
      const dim = R.pathOr({}, [0, 'dimension'], headerRow);
      R.forEach(sheetIndex => {
        workbook
          .sheet(sheetIndex)
          .range(
            `${getPosition(rowIndex, startTablePosition)}:${getPosition(
              rowIndex,
              startTablePosition + rowDimensionsLength,
            )}`,
          )
          .merged(true)
          .style(headerStyles.title)
          .value(dim.label);
      })(SHEETS_INDEX);

      registerSequenceInColumn(
        workbook,
        rowIndex,
        startTablePosition + rowDimensionsLength + spaceLength,
        R.pluck('value', headerRow),
        headerStyles.value,
      );

      rowIndex++;
    }, transposedHeaderData);

    // row header | dim1 | ... | dimn| | col1Flags| ... | colnFlags |
    R.forEach(sheetIndex => {
      registerSequenceInColumn(
        workbook,
        rowIndex,
        startTablePosition,
        rowDimensions,
        rowStyles.rowTitle,
        sheetIndex,
        true,
      );
    })(SHEETS_INDEX);
    // space
    registerCell(
      workbook,
      getPosition(rowIndex, startTablePosition + rowDimensionsLength),
      {},
      rowStyles.space,
    );
    registerSequenceInColumn(
      workbook,
      rowIndex,
      startTablePosition + rowDimensionsLength + spaceLength,
      headerData,
      rowStyles.space,
    );
    rowIndex++;

    /*
      sections
      | section dim 1 val                                                         | section flags|
      |       ...                                                                 |              |
      | section dim X val                                                         |              |
      | row 1 dim 1 val | ... | row 1 dim Y val | row 1 flags | row 1 cell 1| ... | row 1 cell Z |
                                ...
      | row N dim 1 val | ... | row N dim Y val| row N flags | row N cell 1 | ... | row N cell Z |
    */
    // const sectionIntersectionSize = tableFullLength - 1;
    let sectionRowIndexes = [];
    R.forEach(([section, rows]) => {
      if (!R.isEmpty(R.propOr([], 'data', section))) {
        const sectionData = formatSection(section.data);
        registerSequenceInRow(
          workbook,
          rowIndex,
          startTablePosition,
          sectionData,
          {
            ...sectionStyles,
            border: { left: true },
          },
          0,
          false,
        );

        const beginSectionIndex = rowIndex;

        R.forEach(() => {
          sectionRowIndexes = R.append(rowIndex)(sectionRowIndexes);
          R.forEach(sheetIndex => {
            workbook
              .sheet(sheetIndex)
              .range(
                `${getPosition(rowIndex, startTablePosition)}:${getPosition(
                  rowIndex,
                  startTablePosition + tableFullLength - 1,
                )}`,
              )
              .merged(true)
              .style(sectionStyles);
            workbook
              .sheet(sheetIndex)
              .range(
                `${getPosition(
                  beginSectionIndex,
                  startTablePosition + tableFullLength,
                )}:${getPosition(
                  beginSectionIndex + R.length(sectionData) - 1,
                  startTablePosition + tableFullLength,
                )}`,
              )
              .merged(true)
              .style({
                ...sectionStyles,
                border: { right: true },
              });
          })(SHEETS_INDEX);
          rowIndex++;
        })(sectionData);

        registerCell(
          workbook,
          getPosition(beginSectionIndex, startTablePosition + tableFullLength),
          section,
          {
            horizontalAlignment: isRtl ? 'left' : 'right',
            verticalAlignment: 'top',
          },
        );
      }

      R.forEach(row => {
        registerSequenceInColumn(
          workbook,
          rowIndex,
          startTablePosition,
          R.pluck('value', row.data),
          rowStyles.title,
        );

        registerCell(
          workbook,
          getPosition(rowIndex, startTablePosition + rowDimensionsLength),
          row,
          rowStyles.space,
        );

        registerSequenceInColumn(
          workbook,
          rowIndex,
          startTablePosition + rowDimensionsLength + spaceLength,
          R.map(col => R.pathOr({}, [col.key, section.key, row.key, 0], cells), headerData),
          rowStyles.value,
        );
        rowIndex++;
      }, rows);
    }, sectionsData);

    const endRowIndex = rowIndex - 1;
    workbook
      .sheet(0)
      .cell(getPosition(rowIndex + 1, startTablePosition))
      .value(`© ${R.path(['terms', 'label'], footerProps)}`)
      .style({ fontColor: '0563c1', underline: true })
      .hyperlink(R.path(['terms', 'link'], footerProps));

    workbook
      .sheet(0)
      .cell(getPosition(rowIndex + 1, startTablePosition + tableFullLength + 1))
      .value(R.path(['source', 'label'], footerProps))
      .style({ fontColor: '0563c1', underline: true })
      .hyperlink(R.path(['source', 'link'], footerProps));

    const maxStringLengthColRange = columnIndex =>
      workbook
        .sheet(0)
        .range(
          `${getPosition(beginRowIndex, startTablePosition + columnIndex)}:${getPosition(
            endRowIndex,
            startTablePosition + tableFullLength + columnIndex,
          )}`,
        )
        .reduce((max, cell) => {
          const value = cell.value();
          if (R.isNil(value)) return max;
          if (!cell.contentShouldBeFitToCell) return max;
          return Math.max(max, R.pipe(R.toString, R.length, R.add(2))(value));
        }, 0);

    const maxStringLengthRowRange = sectionRowIndex =>
      workbook
        .sheet(0)
        .range(
          `${getPosition(sectionRowIndex, startTablePosition)}:${getPosition(
            sectionRowIndex,
            startTablePosition + tableFullLength - 1,
          )}`,
        )
        .reduce((max, cell) => {
          const value = cell.value();
          if (value === undefined) return max;
          return Math.max(max, value.toString().length);
        }, 0);

    // Set width for columns
    let maxWidths = [];
    R.forEach(columnIndex => {
      const width = maxStringLengthColRange(columnIndex);
      maxWidths = R.append(width)(maxWidths);
      workbook
        .sheet(0)
        .column(startTablePosition + columnIndex)
        .width(R.ifElse(R.flip(R.gt)(70), R.always(70), R.identity)(width));
    })(R.times(R.identity, tableFullLength + spaceLength));

    // Set height for sections only
    const lengthOfFirstColumn = R.head(maxWidths);
    R.forEach(sectionRowIndex => {
      workbook
        .sheet(0)
        .row(sectionRowIndex)
        .height(
          R.pipe(
            R.flip(R.divide)(lengthOfFirstColumn),
            Math.floor,
            R.ifElse(R.flip(R.lt)(1), R.always(1), R.identity),
            R.multiply(15), // height for one line
          )(maxStringLengthRowRange(sectionRowIndex)),
        );
    })(sectionRowIndexes);

    workbook.activeSheet(SHEET1);
    return workbook;
  });
};
