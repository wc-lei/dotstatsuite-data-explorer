import * as R from 'ramda';

export const getPosition = (row, column) => {
  let id = '';
  for (let a = 1, b = 26; (column -= a) >= 0; a = b, b *= 26) {
    id = String.fromCharCode(parseInt((column % b) / a) + 65) + id;
  }
  return `${id}${row}`;
};

export const formatCellValue = ({ intValue, header, label }) => {
  if (!R.isNil(intValue)) {
    return intValue;
  }
  return R.isNil(header) ? label : `${header} ${label}`;
};

export const formatFlags = R.pipe(
  R.map(({ header, label, sub }) => {
    const formattedSelf = R.isNil(header) ? label : `${header} ${label}`;
    const formattedSub = R.ifElse(R.isNil, R.always([]), R.pipe(formatFlags, R.of))(sub);
    return R.prepend(formattedSelf, formattedSub);
  }),
  R.unnest,
  R.join('\n'),
);

export const formatSection = R.map(entry => ({
  label: `${R.pathOr('', ['dimension', 'label'], entry)}: ${R.pathOr(
    '',
    ['value', 'label'],
    entry,
  )}`,
  flags: R.pathOr([], ['value', 'flags'], entry),
}));
