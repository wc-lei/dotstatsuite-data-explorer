import { formatFlags } from '../utils';

describe('export excel formatFlags tests', () => {
  test('simple case', () => {
    expect(formatFlags([{ label: 'first entry' }, { label: 'second entry' }])).toBe(
      'first entry\nsecond entry',
    );
  });
  test('with sub flags and header test', () => {
    expect(
      formatFlags([
        {
          label: 'first entry',
          sub: [
            { header: 'Sub:', label: 'first child' },
            { header: 'Sub:', label: 'second child' },
          ],
        },
        { header: 'Flag:', label: 'second entry' },
      ]),
    ).toBe('first entry\nSub: first child\nSub: second child\nFlag: second entry');
  });
});
