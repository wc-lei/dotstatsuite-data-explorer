import numeral from 'numeral';
import * as R from 'ramda';
import React from 'react';
import { FormattedMessage as RIFormattedMessage } from 'react-intl';

import '@formatjs/intl-pluralrules/polyfill';
import '@formatjs/intl-pluralrules/dist/locale-data/ar';
import '@formatjs/intl-pluralrules/dist/locale-data/en';
import '@formatjs/intl-pluralrules/dist/locale-data/fr';
import '@formatjs/intl-pluralrules/dist/locale-data/es';
import '@formatjs/intl-pluralrules/dist/locale-data/it';
import '@formatjs/intl-pluralrules/dist/locale-data/km';
import '@formatjs/intl-pluralrules/dist/locale-data/nl';

// https://fr.wiktionary.org/wiki/Wiktionnaire:BCP_47/language-2

/*// For IE11: yarn add @formatjs/intl-pluralrules
if (!Intl.PluralRules) {
  import '@formatjs/intl-pluralrules/polyfill';
  import '@formatjs/intl-pluralrules/dist/locale-data/ar';
  import '@formatjs/intl-pluralrules/dist/locale-data/en';
  import '@formatjs/intl-pluralrules/dist/locale-data/fr';
  import '@formatjs/intl-pluralrules/dist/locale-data/es';
  import '@formatjs/intl-pluralrules/dist/locale-data/it';
  import '@formatjs/intl-pluralrules/dist/locale-data/km';
  import '@formatjs/intl-pluralrules/dist/locale-data/nl';
}*/

/*// For IE11: yarn add @formatjs/intl-relativetimeformat
if (!Intl.RelativeTimeFormat) {
  import '@formatjs/intl-relativetimeformat/polyfill';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/ar';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/en';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/fr';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/es';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/it';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/km';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/nl';
}*/

const model = locale => `${locale}/${locale}`;

export const setLocale = locale => numeral.locale(model(locale));

export const initialize = ({ locales = [] }) => {
  R.forEach(locale => {
    const delimiters = R.prop('delimiters')(locale);
    if (R.isNil(delimiters)) return;
    numeral.register('locale', model(R.prop('id')(locale)), { delimiters });
  }, R.values(locales));
};

export { default as I18nProvider } from './provider';

const richValues = {
  br: <br />, // new line -> {br}
  i: chunks => <i>{chunks}</i>, // italic   -> <i>...</i>
  b: chunks => <b>{chunks}</b>, // bold     -> <b>...</b>
};

export const FormattedMessage = ({ values = {}, ...rest }) => (
  <RIFormattedMessage {...rest} values={{ ...values, ...richValues }} />
);

export const formatMessage = intl => (message, values = {}) =>
  intl.formatMessage(message, { ...richValues, ...values });
