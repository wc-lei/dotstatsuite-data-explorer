import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';

const Loader = props => (
  <Grid container justify="center">
    <Grid item>
      <CircularProgress disableShrink color="primary" size={80} {...props} />
    </Grid>
  </Grid>
);

export default Loader;
