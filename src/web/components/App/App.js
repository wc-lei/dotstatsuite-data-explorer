import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '../visions/SisccAppBar';
import Footer from '../footer';
import Loader from '../loader';
import Page from '../Page';
import { ID_AUTH_PAGE } from '../../css-api';
import { getAsset, locales } from '../../lib/settings';
import { getLocale, getIsRtl } from '../../selectors/router';
import { changeLocale } from '../../ducks/app';

const useStyles = makeStyles({
  root: {
    minHeight: '100vh',
  },
  content: {
    flexGrow: 1,
  },
});

const Component = ({ isFull, isAuthenticating, children }) => {
  const classes = useStyles({ isAuthenticating });
  const dispatch = useDispatch();
  const localeId = useSelector(getLocale);
  const isRtl = useSelector(getIsRtl);

  return (
    <Grid container wrap="nowrap" direction="column" className={classes.root}>
      {R.not(isFull) && (
        <Grid item>
          <AppBar
            logo={getAsset('header')}
            locales={R.keys(locales)}
            logoLink={getAsset('logoLink')}
            localeId={localeId}
            isRtl={isRtl}
            changeLocale={localeId => dispatch(changeLocale(localeId))}
          />
        </Grid>
      )}
      {isAuthenticating && (
        <Grid item className={classes.content}>
          <Page id={ID_AUTH_PAGE}>
            <Loader style={{ marginTop: 200 - 64 }} />
          </Page>
        </Grid>
      )}
      {R.not(isAuthenticating) && children}
      {R.not(isFull) && (
        <Grid item>
          <Footer />
        </Grid>
      )}
    </Grid>
  );
};

Component.propTypes = {
  isFull: PropTypes.bool,
  isAuthenticating: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.array, PropTypes.element]),
};

export default Component;
