import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, onlyUpdateForKeys, withProps } from 'recompose';
import * as R from 'ramda';
import ToolBar from './ToolBar';
import { TableLayout, ChartsConfig, DataEdit } from '@sis-cc/dotstatsuite-visions';
import Paper from '@material-ui/core/Paper';
import Collapse from '@material-ui/core/Collapse';
import { useTheme, makeStyles } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import { FormattedMessage, formatMessage } from '../i18n';
import { changeActionId, changeLayout, changeTimeDimensionOrders } from '../ducks/vis';
import {
  getVisActionId,
  getIsSharing,
  getVisDimensionLayout,
  getVisDimensionFormat,
} from '../selectors';
import { getTimePeriod } from '../selectors/sdmx';
import { getViewer, getHasAccessibility, getTimeDimensionOrders } from '../selectors/router';
import { getIsAuthenticated } from '../selectors/user';
import APIQueries from './vis/api-queries';
import ShareView from './vis/share';
import messages from './messages';

const useStyles = makeStyles(theme => ({
  container: {
    margin: theme.spacing(1, 0),
    padding: theme.spacing(2),
  },
}));

//-----------------------------------------------------------------------------------------Constants
const API = 'api';
const TABLE = 'table';
const FILTERS = 'filters';
const CONFIG = 'config';
const SHARE = 'share';

//-----------------------------------------------------------------------------------------ToolTable
export const ToolTable = compose(
  connect(
    createStructuredSelector({
      layout: getVisDimensionLayout,
      itemRenderer: getVisDimensionFormat(),
      itemButton: getTimeDimensionOrders,
      timePeriod: getTimePeriod,
      accessibility: getHasAccessibility,
    }),
    { changeLayout, changeTimeDimensionOrders },
  ),
  injectIntl,
  onlyUpdateForKeys(['layout', 'timePeriod', 'itemRenderer', 'itemButton', 'accessibility']),
)(
  ({
    layout,
    changeLayout,
    itemButton,
    itemRenderer,
    timePeriod,
    intl,
    changeTimeDimensionOrders,
    accessibility,
  }) => {
    const asc = formatMessage(intl)(messages.asc);
    const desc = formatMessage(intl)(messages.desc);
    const timePeriodId = R.propOr('', 'id')(timePeriod);
    const isTimeDimensionInverted = R.propOr(false, timePeriodId)(itemButton);
    const timePeriodButton = R.pipe(
      R.set(R.lensProp('value'), isTimeDimensionInverted ? desc : asc),
      R.set(R.lensProp('options'), [asc, desc]),
      R.set(R.lensProp('onChange'), v =>
        changeTimeDimensionOrders(timePeriodId, R.equals(desc)(v)),
      ),
    )(timePeriod);
    return (
      <TableLayout
        accessibility={accessibility}
        layout={layout}
        commit={changeLayout}
        itemButton={itemButton}
        itemRenderer={itemRenderer}
        itemButtonProps={{
          [timePeriodId]: timePeriodButton,
        }}
        labels={{
          commit: <FormattedMessage id="de.table.layout.apply" />,
          cancel: <FormattedMessage id="de.table.layout.cancel" />,
          row: <FormattedMessage id="de.table.layout.x" />,
          column: <FormattedMessage id="de.table.layout.y" />,
          section: <FormattedMessage id="de.table.layout.z" />,
          d: <FormattedMessage id="de.table.layout.getter.dimension" />,
          o: <FormattedMessage id="de.table.layout.getter.observation" />,
          time: <FormattedMessage id="de.table.layout.time" />,
          asc: <FormattedMessage id="de.table.layout.time.asc" />,
          desc: <FormattedMessage id="de.table.layout.time.desc" />,
          help: <FormattedMessage id="de.table.layout.help" />,
          table: <FormattedMessage id="de.table.layout.table" />,
          one: <FormattedMessage id="de.table.layout.value.one" />,
          wcagDragStart: props => formatMessage(intl)(messages.start, props),
          wcagDragUpdate: props => formatMessage(intl)(messages.update, props),
          wcagDragEnd: props => formatMessage(intl)(messages.end, props),
          wcagDragCancel: formatMessage(intl)(messages.cancel),
          wcagDragExplanation: formatMessage(intl)(messages.explanation),
        }}
      />
    );
  },
);

//---------------------------------------------------------------------------------------ChartConfig
const renameKey = R.curry((oldKey, newKey, obj) =>
  R.assoc(newKey, R.prop(oldKey, obj), R.dissoc(oldKey, obj)),
);

const renameProperties = (keys = []) =>
  R.map(
    R.ifElse(
      R.pipe(R.prop('id'), R.flip(R.includes)(keys)),
      renameKey('onChange', 'onSubmit'),
      R.identity,
    ),
  );

const propertiesKeys = [
  'width',
  'height',
  'freqStep',
  'maxX',
  'minX',
  'pivotX',
  'stepX',
  'maxY',
  'minY',
  'pivotY',
  'stepY',
  'title',
  'subtitle',
  'source',
];

const EnhancedChartsConfig = ({ isAuthenticated, labels, properties }) => {
  if (isAuthenticated) {
    return (
      <div>
        <ChartsConfig labels={labels} properties={properties} />
        <DataEdit labels={labels} properties={properties} />
      </div>
    );
  }
  return <ChartsConfig labels={labels} properties={properties} />;
};

EnhancedChartsConfig.propTypes = {
  isAuthenticated: PropTypes.bool,
  labels: PropTypes.object,
  properties: PropTypes.object,
};

export const Config = compose(
  injectIntl,
  withProps(({ intl, isAuthenticated, properties }) => {
    const labels = {
      focus: formatMessage(intl)(messages.focus),
      highlight: formatMessage(intl)(messages.highlight),
      select: formatMessage(intl)(messages.select),
      baseline: formatMessage(intl)(messages.baseline),
      size: formatMessage(intl)(messages.size),
      width: formatMessage(intl)(messages.width),
      height: formatMessage(intl)(messages.height),
      display: formatMessage(intl)(messages.display),
      displayOptions: {
        label: formatMessage(intl)(messages.label),
        code: formatMessage(intl)(messages.code),
        both: formatMessage(intl)(messages.both),
      },
      series: formatMessage(intl)(messages.series),
      scatterDimension: formatMessage(intl)(messages.scatterDimension),
      scatterX: formatMessage(intl)(messages.scatterX),
      scatterY: formatMessage(intl)(messages.scatterY),
      symbolDimension: formatMessage(intl)(messages.symbolDimension),
      stackedDimension: formatMessage(intl)(messages.stackedDimension),
      stackedMode: formatMessage(intl)(messages.stackedMode),
      stackedModeOptions: {
        values: formatMessage(intl)(messages.values),
        percent: formatMessage(intl)(messages.percent),
      },
      axisX: formatMessage(intl)(messages.axisX),
      axisY: formatMessage(intl)(messages.axisY),
      max: formatMessage(intl)(messages.max),
      min: formatMessage(intl)(messages.min),
      pivot: formatMessage(intl)(messages.pivot),
      step: formatMessage(intl)(messages.step),
      frequency: formatMessage(intl)(messages.frequency),
      freqStep: formatMessage(intl)(messages.freqStep),
      title: formatMessage(intl)(messages.configTitle),
      subtitle: formatMessage(intl)(messages.subtitle),
      source: formatMessage(intl)(messages.source),
      logo: formatMessage(intl)(messages.logo),
      copyright: formatMessage(intl)(messages.copyright),
      reset: formatMessage(intl)(messages.reset),
      uniqFocusOption: formatMessage(intl)(messages.uniqFocusOption)
    };
    return {
      labels,
      properties: R.pipe(
        renameProperties(propertiesKeys),
        R.when(
          R.always(!isAuthenticated),
          R.omit(['title', 'subtitle', 'source', 'logo', 'copyright']),
        ),
      )(properties),
    };
  }),
)(EnhancedChartsConfig);

//---------------------------------------------------------------------------------------------Tools
export default compose(
  connect(
    createStructuredSelector({
      actionId: getVisActionId(),
      viewerId: getViewer,
      isSharing: getIsSharing(),
      isAuthenticated: getIsAuthenticated,
    }),
    { changeActionId },
  ),
  withProps(({ actionId, viewerId }) => ({
    isApi: R.equals(API)(actionId),
    isTableConfig: R.equals(CONFIG)(actionId) && R.equals(TABLE)(viewerId),
    isChartConfig: R.equals(CONFIG)(actionId) && !R.equals(TABLE)(viewerId),
    isShare: R.equals(SHARE)(actionId),
    isFilters: R.equals(FILTERS)(actionId),
  })),
)(
  ({
    isAuthenticated,
    isNarrow,
    isRtl,
    isApi,
    isTableConfig,
    isChartConfig,
    isShare,
    properties,
    share,
    viewerProps,
  }) => {
    const classes = useStyles();
    const theme = useTheme();
    return (
      <Fragment>
        <ToolBar viewerProps={viewerProps} theme={theme} />
        <Collapse in={isApi}>
          <Paper elevation={2} className={classes.container}>
            <APIQueries isNarrow={isNarrow} isRtl={isRtl} />
          </Paper>
        </Collapse>
        <Collapse in={isTableConfig}>
          <Paper elevation={2} className={classes.container}>
            <ToolTable isNarrow={isNarrow} isRtl={isRtl} />
          </Paper>
        </Collapse>
        <Collapse in={isChartConfig}>
          <Paper elevation={2} className={classes.container}>
            <Config isAuthenticated={isAuthenticated} properties={properties} />
          </Paper>
        </Collapse>
        <Collapse in={isShare}>
          <Paper elevation={2}>
            <ShareView share={share} />
          </Paper>
        </Collapse>
      </Fragment>
    );
  },
);
