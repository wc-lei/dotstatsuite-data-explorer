import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Divider from '@material-ui/core/Divider';
import { defineMessages, useIntl } from 'react-intl';
import { formatMessage } from '../../../i18n';
import User from '../../user';
import AccessibilityButton from '../../AccessibilityButton';
import useStyles from './useStyles';
import Link from '@material-ui/core/Link';

const messages = defineMessages({
  en: { id: 'en' },
  it: { id: 'it' },
  fr: { id: 'fr' },
  ar: { id: 'ar' },
  es: { id: 'es' },
  km: { id: 'km' },
  nl: { id: 'nl' },
});

const Component = ({ logo, locales, localeId, changeLocale, logoLink }) => {
  const classes = useStyles();
  const intl = useIntl();

  return (
    <AppBar data-testid="sisccappbar" position="static" className={classes.appBar} elevation={0}>
      <Toolbar className={classes.toolBar}>
        <div className={classes.logoWrapper}>
          {logoLink ? (
            <Link href={logoLink} target="_blank" rel="noopener noreferrer">
              {' '}
              <img className={classes.logo} src={logo} alt="siscc logo" />{' '}
            </Link>
          ) : (
            <img className={classes.logo} src={logo} alt="siscc logo" />
          )}
        </div>
        <AccessibilityButton />
        <Divider orientation="vertical" className={classes.divider} />
        <User />
        <Divider orientation="vertical" className={classes.divider} />
        <TextField
          select
          value={localeId}
          variant="outlined"
          onChange={event => changeLocale(event.target.value)}
          SelectProps={{
            classes: { root: classes.select },
            MenuProps: {
              getContentAnchorEl: null,
              anchorOrigin: { vertical: 'bottom', horizontal: 'center' },
              transformOrigin: { vertical: 'top', horizontal: 'center' },
              classes: { paper: classes.paper },
            },
          }}
        >
          {R.map(id => (
            <MenuItem key={id} value={id} dense className={classes.menuItem}>
              <span>{formatMessage(intl)(messages[id])}</span>
            </MenuItem>
          ))(locales)}
        </TextField>
      </Toolbar>
    </AppBar>
  );
};

Component.propTypes = {
  logo: PropTypes.string,
  locales: PropTypes.array.isRequired,
  localeId: PropTypes.string,
  changeLocale: PropTypes.func.isRequired,
  logoLink: PropTypes.string,
};

export default Component;
