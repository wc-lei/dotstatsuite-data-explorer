import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from '../i18n';
import { withProps } from 'recompose';
import Link from '@material-ui/core/Link';
import { SisccFooter } from '@sis-cc/dotstatsuite-visions';
import { makeStyles } from '@material-ui/core';
import { getAsset } from '../lib/settings';

const useStyles = makeStyles(() => ({
  img: {
    maxHeight: 20,
    verticalAlign: 'middle',
  },
}));

const Footer = ({ logo }) => {
  const classes = useStyles();
  return (
    <SisccFooter
      leftLabel={
        <FormattedMessage
          id="de.footer.author"
          values={{
            icon: <img className={classes.img} src={logo} alt="icon" />,
            link: (
              <Link underline="hover" color="primary" href="https://siscc.org/">
                SIS-CC
              </Link>
            ),
          }}
        />
      }
      rightLabel={<FormattedMessage id="de.footer.disclaimer" />}
    />
  );
};

Footer.propTypes = {
  logo: PropTypes.string,
};

export default withProps({ logo: getAsset('footer') })(Footer);
