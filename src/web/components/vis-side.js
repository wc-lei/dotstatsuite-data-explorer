import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, withProps, pure, branch, renderNothing } from 'recompose';
import * as R from 'ramda';
import { injectIntl } from 'react-intl';
import { formatMessage } from '../i18n';
import {
  Chips,
  ScopeList,
  ExpansionPanel,
  PeriodPicker,
  InputNumber,
  Tag,
  Spotlight,
  spotlightScopeListEngine,
} from '@sis-cc/dotstatsuite-visions';
import { rules } from '@sis-cc/dotstatsuite-components';
import { PanelContentConstraints, ActiveContentConstraints } from './vis-side/content-constraints';
import {
  changeFilter,
  changeDataquery,
  changeFrequencyPeriod,
  changeLastNObservations,
  resetFilters,
  resetSpecialFilters,
} from '../ducks/sdmx';
import { PANEL_PERIOD, PANEL_USED_FILTERS } from '../utils/constants';
import {
  getFilter,
  getLastNObservations,
  getHasLastNObservations,
  getLocale,
  getFrequency,
  getHasAccessibility,
} from '../selectors/router';
import {
  getFilters,
  getSelection,
  getFrequencyArtefactContentConstrained,
  getPeriod,
  getTimePeriod,
  getDatesBoundaries,
  getAvailableFrequencies,
  getNotApplyDataAvailabilty,
} from '../selectors/sdmx';
import { getIntervalPeriod, getDateFromSdmxPeriod } from '../lib/sdmx/frequency.js';
import { countNumberOf } from '../utils';
import {
  START_PERIOD,
  END_PERIOD,
  LASTN,
  addI18nLabels,
  getUsedFilterPeriod,
  getUsedFilterFrequency,
} from '../utils/used-filter';
import { locales } from '../lib/settings';
import messages, { periodMessages } from './messages';

const withFilterFrequency = withProps(({ frequency, frequencyArtefact, availableFrequencies }) => ({
  frequencyFilter: getUsedFilterFrequency(frequency, availableFrequencies)(frequencyArtefact),
}));

const withFilterPeriod = withProps(({ intl, period, lastN, locale, timePeriod }) => ({
  periodFilter: getUsedFilterPeriod(
    period,
    lastN,
    rules.getTimePeriodLabel(locale, R.path([locale, 'timeFormat'])(locales)),
    timePeriod,
  ),
  periodLabels: {
    [START_PERIOD]: formatMessage(intl)(messages.periodStart),
    [END_PERIOD]: formatMessage(intl)(messages.periodEnd),
    [LASTN]: [formatMessage(intl)(messages.periodLast), formatMessage(intl)(messages.periods)],
  },
}));

const withDatesFromSdmxPeriod = withProps(({ period, frequency }) => ({
  dates: getDateFromSdmxPeriod(frequency, period),
}));

//--------------------------------------------------------------------------------------------------
export const UsedFilters = compose(
  connect(
    createStructuredSelector({
      items: getSelection,
    }),
    { onDelete: changeDataquery },
  ),
  pure,
)(Chips);

export const SpecialUsedFilters = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      frequency: getFrequency,
      frequencyArtefact: getFrequencyArtefactContentConstrained,
      period: getPeriod,
      lastN: getLastNObservations,
      locale: getLocale,
      timePeriod: getTimePeriod,
    }),
    {
      onDeleteAll: resetFilters,
      onDelete: resetSpecialFilters,
    },
  ),
  withFilterFrequency,
  withFilterPeriod,
  withProps(({ intl, frequencyFilter = [], periodFilter = [], periodLabels }) => ({
    items: R.concat(frequencyFilter, addI18nLabels(periodLabels)(periodFilter)),
    clearAllLabel: formatMessage(intl)(messages.clear),
  })),
  pure,
)(Chips);

export const FiltersCurrent = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      items1: getSelection,
      frequency: getFrequency,
      frequencyArtefact: getFrequencyArtefactContentConstrained,
      period: getPeriod,
      lastN: getLastNObservations,
      locale: getLocale,
      timePeriod: getTimePeriod,
    }),
    { onChangeActivePanel: changeFilter },
  ),
  withFilterFrequency,
  withFilterPeriod,
  withProps(({ intl, items1 = [], frequencyFilter = [], periodFilter = [], activePanelId }) => ({
    isOpen: R.equals(PANEL_USED_FILTERS, activePanelId),
    id: PANEL_USED_FILTERS,
    label: formatMessage(intl)(messages.title),
    tag: <Tag>{countNumberOf(R.concat(items1, R.concat(frequencyFilter, periodFilter)))}</Tag>,
  })),
)(ExpansionPanel);

//--------------------------------------------------------------------------------------------------
const Filter = pure(ScopeList);
export const Filters = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      filters: getFilters,
      activePanelId: getFilter,
      notApplyDataAvailabilty: getNotApplyDataAvailabilty,
      accessibility: getHasAccessibility,
    }),
    { changeSelection: changeDataquery, onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl }) => ({
    labels: {
      navigateNext: formatMessage(intl)(messages.next),
      navigateBefore: formatMessage(intl)(messages.before),
    },
    topElementProps: {
      fullWidth: true,
      hasClearAll: true,
      mainPlaceholder: formatMessage(intl)(messages.primary),
      secondaryPlaceholder: formatMessage(intl)(messages.secondary),
      defaultSpotlight: {
        engine: spotlightScopeListEngine,
        placeholder: formatMessage(intl)(messages.placeholder),
        fields: {
          label: {
            id: 'label',
            accessor: R.pipe(R.propOr(null, 'label'), R.ifElse(R.isNil, R.always(''), R.identity)),
            isSelected: true,
          },
        },
      },
    },
    topElementComponent: values => (R.gte(R.length(values), 8) ? Spotlight : null),
  })),
  pure,
)(({ topElementComponent, filters, notApplyDataAvailabilty, ...parentProps }) =>
  R.map(
    ({ id, label, values = [] }) => (
      <Filter
        {...parentProps}
        limitDisplay={1}
        displayAccessor={notApplyDataAvailabilty ? undefined : R.prop('hasData')}
        id={id}
        key={id}
        label={label}
        items={values}
        TopElementComponent={topElementComponent(values)}
      />
    ),
    filters,
  ),
);

//--------------------------------------------------------------------------------------------------
export const Period = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      period: getPeriod,
      frequency: getFrequency,
      availableFrequencies: getAvailableFrequencies,
      boundaries: getDatesBoundaries,
    }),
    { changeFrequencyPeriod },
  ),
  withDatesFromSdmxPeriod,
  withProps(
    ({ availableFrequencies, frequency, dates, changeFrequencyPeriod, intl, boundaries }) => ({
      defaultFrequency: frequency,
      availableFrequencies: R.keys(availableFrequencies),
      boundaries,
      period: dates,
      labels: R.mergeRight(
        R.reduce(
          (memo, [id, message]) => R.assoc(id, formatMessage(intl)(message), memo),
          {},
          R.toPairs(periodMessages),
        ),
      )(availableFrequencies),
      changeFrequency: frequency =>
        changeFrequencyPeriod({
          valueId: frequency,
          dates,
        }),
      changePeriod: dates => changeFrequencyPeriod({ valueId: frequency, dates }),
    }),
  ),
  pure,
)(PeriodPicker);

export const LastNPeriod = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      value: getLastNObservations,
      hasLastNObservations: getHasLastNObservations,
    }),
    { onChange: changeLastNObservations },
  ),
  withProps(({ intl }) => ({
    beforeLabel: formatMessage(intl)(messages.beforeLabel),
    afterLabel: formatMessage(intl)(messages.afterLabel),
    popperLabel: formatMessage(intl)(messages.popperLabel),
  })),
  branch(({ hasLastNObservations }) => R.not(hasLastNObservations), renderNothing),
)(InputNumber);

export const FilterPeriod = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      activePanelId: getFilter,
      period: getPeriod,
      frequency: getFrequency,
      availableFrequencies: getAvailableFrequencies,
      datesBoundaries: getDatesBoundaries,
    }),
    { changeFilter, onChangeActivePanel: changeFilter },
  ),
  withProps(({ intl, activePanelId, period, frequency, availableFrequencies, datesBoundaries }) => {
    const dates = getDateFromSdmxPeriod(frequency, period);
    return {
      isBlank: R.all(R.isNil)(datesBoundaries),
      label: R.or(
        R.isEmpty(availableFrequencies),
        R.pipe(R.keys, R.length, R.equals(1))(availableFrequencies),
      )
        ? formatMessage(intl)(messages.last)
        : R.join(' & ')([formatMessage(intl)(messages.head), formatMessage(intl)(messages.last)]),
      overflow: true,
      id: PANEL_PERIOD,
      isOpen: R.equals(PANEL_PERIOD, activePanelId),
      tag: <Tag>{R.join(' / ')(getIntervalPeriod(datesBoundaries)(frequency, dates))}</Tag>,
    };
  }),
)(ExpansionPanel);

//----------------------------------------------------------------------------------------------Side
const Side = ({ isNarrow, isRtl }) => (
  <Fragment>
    <FiltersCurrent isNarrow={isNarrow} isRtl={isRtl}>
      <UsedFilters />
      <SpecialUsedFilters />
    </FiltersCurrent>
    <FilterPeriod isNarrow={isNarrow} isRtl={isRtl}>
      <Period />
      <LastNPeriod />
    </FilterPeriod>
    <Filters isNarrow={isNarrow} isRtl={isRtl} />
    <PanelContentConstraints>
      <ActiveContentConstraints />
    </PanelContentConstraints>
  </Fragment>
);

Side.propTypes = {
  isRtl: PropTypes.bool,
  isNarrow: PropTypes.bool,
};

export default Side;
