import React from 'react';
import PropTypes from 'prop-types';
import * as R from 'ramda';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { innerPalette } from '@sis-cc/dotstatsuite-visions';
import { getIsRtl, getLocale, getHasAccessibility } from '../selectors/router';
import { getDataflowName } from '../selectors/sdmx';
import { title, outerPalette } from '../lib/settings';

const View = ({ isRtl, lang, dataflowName, isA11y }) => (
  <Helmet htmlAttributes={{ lang, dir: isRtl ? 'rtl' : 'ltr' }}>
    {R.isNil(dataflowName)
      ? <title>{title}</title>
      : <title>{`${dataflowName} • ${title}`}</title>
    }
    { isA11y && <style type="text/css">{`
        :focus {
          outline-color: ${outerPalette.highlight1 || innerPalette.highlight1} !important;
        }
      `}</style>
    }
  </Helmet>
);

View.propTypes = {
  isRtl: PropTypes.bool,
  isA11y: PropTypes.bool,
  lang: PropTypes.string,
  dataflowName: PropTypes.string,
};

export default connect(
  createStructuredSelector({
    isRtl: getIsRtl,
    isA11y: getHasAccessibility,
    lang: getLocale,
    dataflowName: getDataflowName
  }),
)(View);
