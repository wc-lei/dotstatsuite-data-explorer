import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  compose,
  branch,
  renderComponent,
  withProps,
  withStateHandlers,
  lifecycle,
} from 'recompose';
import { injectIntl } from 'react-intl';
import * as R from 'ramda';
import Home from './home';
import Results from './results';
import { getIsPending } from '../../selectors/app';
import {
  getResultsFacets,
  getDataflows,
  getConstraints,
  getConfigFacets,
  getHasNoSearchParams,
  getNumFound,
} from '../../selectors/search';
import {
  requestConfig,
  resetSearch,
  changeTerm,
  requestSearch,
  changeFacet,
  changeConstraints,
} from '../../ducks/search';
import { getTerm, getFacet, getHasAccessibility } from '../../selectors/router';
import { getAsset, hasNoSearch } from '../../lib/settings';
import { setLabel } from './utils';

const EnhancedHome = compose(
  injectIntl,
  connect(
    createStructuredSelector({
      facets: getConfigFacets,
      isLoading: getIsPending('getConfig'),
    }),
    { changeTerm, changeConstraints, requestConfig },
  ),
  withProps(({ facets, intl }) => ({
    hasNoSearch,
    logo: getAsset('splash'),
    facets: R.ifElse(
      R.isEmpty,
      R.identity,
      R.map(
        R.pipe(
          setLabel({ intl }),
          R.over(
            R.lensProp('values'),
            R.ifElse(
              R.isEmpty,
              R.identity,
              R.pipe(
                R.filter(R.pipe(R.prop('level'), R.anyPass([R.isNil, isNaN, R.gte(1)]))),
                R.groupBy(
                  R.pipe(R.prop('parentId'), R.ifElse(R.isNil, R.always('orphans'), R.identity)),
                ),
                R.converge(
                  (orphans, children) =>
                    R.map(orphan =>
                      R.assoc(
                        'subtopics',
                        R.pipe(R.propOr([], R.prop('id', orphan)))(children),
                        orphan,
                      ),
                    )(orphans),
                  [R.prop('orphans'), R.dissoc('orphans')],
                ),
              ),
            ),
          ),
        ),
      ),
    )(facets),
  })),
  lifecycle({
    componentDidMount() {
      this.props.requestConfig();
    },
  }),
)(Home);

export default compose(
  injectIntl,
  connect(
    createStructuredSelector({
      dataflows: getDataflows,
      facets: getResultsFacets,
      constraints: getConstraints,
      isSearchLoading: getIsPending('getSearch'),
      isConfigLoading: getIsPending('getConfig'),
      term: getTerm,
      facet: getFacet,
      size: getNumFound,
      accessibility: getHasAccessibility,
      hasNoSearchParams: getHasNoSearchParams,
    }),
    {
      changeConstraints,
      changeFacet,
      changeTerm,
      resetSearch,
      requestSearch,
    },
  ),
  withStateHandlers(
    { actionId: undefined },
    {
      changeActionId: ({ actionId }) => nextActionId => ({
        actionId: R.ifElse(R.equals(actionId), R.always(null), R.identity)(nextActionId),
      }),
    },
  ),
  withProps(({ dataflows, isConfigLoading, isSearchLoading, constraints, intl }) => ({
    logo: getAsset('subheader'),
    isLoading: R.or(isConfigLoading, isSearchLoading),
    isBlank: R.isEmpty(dataflows),
    constraints: R.map(setLabel({ intl }), constraints),
  })),
  branch(({ hasNoSearchParams }) => hasNoSearchParams, renderComponent(EnhancedHome)),
  lifecycle({
    componentDidMount() {
      this.props.requestSearch();
    },
  }),
)(Results);
