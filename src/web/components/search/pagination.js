import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, branch, renderNothing, withProps } from 'recompose';
import * as R from 'ramda';
import { getCurrentRows, getPage, getPages } from '../../selectors/search';
import { changeStart } from '../../ducks/search';
import { defineMessages, injectIntl } from 'react-intl';
import { Pagination } from '@sis-cc/dotstatsuite-visions';
import { formatMessage } from '../../i18n';

const messages = defineMessages({
  page: { id: 'de.search.page' },
  of: { id: 'de.search.page.of' },
  startPage: { id: 'wcag.search.page.start' },
  previousPage: { id: 'wcag.search.page.previous' },
  nextPage: { id: 'wcag.search.page.next' },
  endPage: { id: 'wcag.search.page.end' },
});

// labels need to be string to support wcag
export default compose(
  injectIntl,
  connect(
    createStructuredSelector({
      page: getPage,
      pages: getPages,
      rows: getCurrentRows,
    }),
    { changeStart },
  ),
  branch(({ pages, rows }) => R.isNil(rows) || R.isNil(pages) || pages === 1, renderNothing),
  withProps(({ changeStart, rows, intl }) => ({
    onChange: page => changeStart(rows * (page - 1)),
    onSubmit: page => changeStart(rows * (page - 1)),
    labels: R.reduce(
      (memo, [id, message]) => R.assoc(id, formatMessage(intl)(message), memo),
      {},
      R.toPairs(messages),
    ),
  })),
)(Pagination);
