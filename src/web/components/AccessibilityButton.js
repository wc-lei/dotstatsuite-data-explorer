import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { FormattedMessage } from '../i18n';
import Button from '@material-ui/core/Button';
import AccessibilityNewIcon from '@material-ui/icons/AccessibilityNew';
import { getHasAccessibility } from '../selectors/router';
import { changeHasAccessibility } from '../ducks/app';

const AccessibilityButton = () => {
  const dispatch = useDispatch();
  const hasAccessibility = useSelector(getHasAccessibility);

  return (
    <Button onClick={() => dispatch(changeHasAccessibility(!hasAccessibility))}>
      <AccessibilityNewIcon />
      {hasAccessibility ? (
        <FormattedMessage id="accessibility.disable" />
      ) : (
        <FormattedMessage id="accessibility.enable" />
      )}
    </Button>
  );
};

export default AccessibilityButton;
