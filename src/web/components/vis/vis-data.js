import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose, mapProps, withProps } from 'recompose';
import * as R from 'ramda';
import { RulesDriver, Viewer as ViewerComp } from '@sis-cc/dotstatsuite-components';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { injectIntl } from 'react-intl';
import { FormattedMessage, formatMessage, withLocale } from '../../i18n';
import Tools from '../vis-tools';
import { share } from '../../ducks/vis';
import {
  getShareMode,
  getVisDataDimensions,
  getVisTableLayout,
  getTableProps,
  getVisChoroMap,
  getVisDataflow,
  getVisIsLoadingMap,
  getIsTimeInverted,
} from '../../selectors';
import { getIsPending } from '../../selectors/app';
import {
  getData,
  getDataUrl,
  getTimelineAxisProc,
  getDataRange,
  getDataSourceHeaders,
  getSelection,
} from '../../selectors/sdmx';
import * as Settings from '../../lib/settings';
import {
  getLocale,
  getIsRtl as routerGetIsRtl,
  getDisplay,
  getViewer,
  getDataflow,
  getHasAccessibility,
} from '../../selectors/router';
import withRefinedViewerProps from '../../utils/viewer';
import { options as viewerOptions } from '../../lib/viewer';
import { getIsRtl } from '../../theme/utils';
import { ID_VIEWER_COMPONENT } from '../../css-api';
import messages from '../messages';

const getHeaderDisclaimer = (range = {}, type) => {
  const { count, total } = R.pick(['count', 'total'], range);
  if (R.any(R.isNil, [count, total])) {
    return null;
  }
  if (count < total) {
    const values = { range: count };
    if (type === 'table') return <FormattedMessage id="incomplete.table.data" values={values} />;
    return <FormattedMessage id="incomplete.chart.data" values={values} />;
  }
  return null;
};

const useStyles = makeStyles(theme => ({
  divider: {
    borderTop: 'solid 2px #999',
    paddingTop: theme.spacing(0.5),
  },
}));

const DataVisView = ({
  isLoadingData,
  isLoadingMap,
  isNarrow,
  isFull,
  toolsProps,
  viewerProps,
  hasSelection,
  hasAccessibility,
}) => {
  const theme = useTheme();
  const classes = useStyles();
  const isRtl = getIsRtl(theme);

  let loading = null;
  if (isLoadingData) loading = <FormattedMessage id="de.visualisation.data.loading" />;
  else if (isLoadingMap) loading = <FormattedMessage id="de.vis.map.loading" />;

  let noData = <FormattedMessage id="vx.no.data.available" />;
  if (hasSelection) noData = <FormattedMessage id="vx.no.data.selection" />;

  return (
    <div className={classes.divider}>
      <Tools
        isNarrow={isNarrow}
        isRtl={isRtl}
        isFull={isFull}
        viewerProps={viewerProps}
        {...toolsProps}
      />
      <div id={ID_VIEWER_COMPONENT}>
        <ViewerComp
          {...viewerProps}
          hasAccessibility={hasAccessibility}
          loading={loading}
          isNarrow={isNarrow}
          isRtl={isRtl}
          noData={noData}
        />
      </div>
      {loading && (
        <Typography tabIndex={0} aria-live="assertive" variant="srOnly">
          <FormattedMessage id="de.visualisation.data.loading" />
        </Typography>
      )}
    </div>
  );
};

const DataVis = compose(
  connect(
    createStructuredSelector({
      dataflow: getDataflow,
      dimensions: getVisDataDimensions(),
      isLoadingData: getIsPending('getData'),
      isLoadingMap: getVisIsLoadingMap,
      isTimeInverted: getIsTimeInverted,
      layoutIds: getVisTableLayout(),
      shareMode: getShareMode(),
      tableProps: getTableProps(),
      range: getDataRange,
      sourceHeaders: getDataSourceHeaders,
      selection: getSelection,
      hasAccessibility: getHasAccessibility,
    }),
    { share },
  ),
  withProps(({ headerProps, footerProps, range, type }) => ({
    fonts: viewerOptions.fonts,
    footerProps: R.pipe(
      R.assoc('source', { label: R.prop('sourceLabel', footerProps), link: window.location.href }),
      R.when(R.always(!R.prop('withLogo', footerProps)), R.dissoc('logo')),
      R.when(R.always(!R.prop('withCopyright', footerProps)), R.omit(['owner', 'terms'])),
    )(R.omit(['fonts'], viewerOptions)),
    headerProps: R.assoc('disclaimer', getHeaderDisclaimer(range, type))(headerProps),
  })),
  withProps(({ data, dataflow, locale, sdmxUrl, tableProps, selection }) => ({
    cellsLimit: Settings.cellsLimit,
    customAttributes: Settings.customAttributes,
    dataflowId: dataflow.dataflowId,
    isNotATable: R.hasPath(['cells', '', '', '', 0], tableProps),
    source: sdmxUrl,
    isNonIdealState: R.isNil(data),
    locale: R.pathOr({ id: locale }, ['i18n', 'locales', locale], Settings),
    hasSelection: R.not(R.isEmpty(selection)),
  })),
  withRefinedViewerProps,
)(DataVisView);

export const ParsedDataVisualisation = compose(
  connect(
    createStructuredSelector({
      display: getDisplay,
      type: getViewer,
      data: getData,
      dataflow: getVisDataflow,
      timelineAxisProc: getTimelineAxisProc,
      dataUrl: getDataUrl({ agnostic: false }),
      map: getVisChoroMap,
      locale: getLocale,
      isRtl: routerGetIsRtl,
    }),
  ),
  injectIntl,
  mapProps(({ dataUrl, intl, timelineAxisProc, map, type, ...rest }) => {
    const isNonIdealMapState = R.allPass([
      R.always(type === 'ChoroplethChart'),
      R.anyPass([R.isNil, R.has('error')]),
    ])(map);
    return {
      customAttributes: Settings.customAttributes,
      sdmxUrl: dataUrl,
      formaterIds: {
        decimals: Settings.getSdmxAttribute('decimals'),
        prefscale: Settings.getSdmxAttribute('prefscale'),
      },
      options: R.when(
        R.always(type === 'TimelineChart'),
        R.set(R.lensPath(['axis', 'x', 'format', 'proc']), timelineAxisProc),
      )(Settings.chartOptions),
      source: { link: window.location.href },
      map: R.when(R.always(isNonIdealMapState), R.always(null))(map),
      type,
      isNonIdealMapState,
      units: {
        ...Settings.units,
        unitDimension: {
          id: R.prop('id', Settings.units),
          name: formatMessage(intl)(messages.unitsOfMeasure)
        }
      },
      ...rest,
    };
  }),
)(props => (
  <RulesDriver
    {...props}
    render={(
      parsedProps, // chartData, chartOptions, properties (aka chart configs props)
    ) => {
      return (
        <DataVis
          {...parsedProps}
          {...R.pick(
            ['data', 'display', 'formaterIds', 'locale', 'type', 'isNonIdealMapState', 'sdmxUrl'],
            props,
          )}
        />
      );
    }}
  />
));
