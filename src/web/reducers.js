import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import app from './ducks/app';
import search from './ducks/search';
import vis from './ducks/vis';
import sdmx from './ducks/sdmx';
import user from './ducks/user';

export default history =>
  combineReducers({
    app,
    search,
    sdmx,
    vis,
    user,
    router: connectRouter(history),
  });
