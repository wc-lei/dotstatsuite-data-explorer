import { createBrowserHistory } from 'history';
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'connected-react-router';
import createRootReducer from './reducers';
import rootSaga from './sagas';
import { requestMiddleware, analyticsMiddleware, historyMiddleware } from './middlewares';

const isDev = process.env.NODE_ENV === 'development';

const isInBrowser = typeof window !== 'undefined';
export const history = isInBrowser ? createBrowserHistory() : {};

export default (initialState, { gaToken }) => {
  const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [
    thunk,
    sagaMiddleware,
    historyMiddleware,
    requestMiddleware,
    routerMiddleware(history),
  ];

  if (gaToken) middlewares.push(analyticsMiddleware);

  if (isDev) {
    const { createLogger } = require('redux-logger');
    const logger = createLogger({ duration: true, timestamp: false, collapsed: true, diff: true });
    middlewares.push(logger);
  }

  const store = createStore(
    createRootReducer(history),
    initialState,
    composeEnhancer(applyMiddleware(...middlewares)),
  );

  sagaMiddleware.run(rootSaga);
  return store;
};
