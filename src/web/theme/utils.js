import * as R from 'ramda';

export const getIsRtl = theme => R.equals('rtl', R.prop('direction', theme));

export const getLight = R.path(['palette', 'tertiary', 'light']);
export const getDark = R.path(['palette', 'tertiary', 'dark']);
export const getFont = prop => R.path(['mixins', 'excel', prop]);
