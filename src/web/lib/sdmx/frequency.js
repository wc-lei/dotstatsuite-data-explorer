import * as R from 'ramda';
import dateFns from 'date-fns';
import { defaultFrequency } from '../settings';

const sdmxFormat = {
  A: 'YYYY',
  S: 'YYYY-[S][SEMESTER]',
  Q: 'YYYY-[Q]Q',
  M: 'YYYY-MM',
  W: 'YYYY-[W]WW',
  B: 'YYYY-[W]WW',
  D: 'YYYY-MM-DD',
  H: 'YYYY-MM-DD[T]HH:mm:ss',
  N: 'YYYY-MM-DD[T]HH:mm:ss',
};

const dateWithoutTZ = date => dateFns.addMinutes(date, date.getTimezoneOffset());

const getIsValidDate = dateStr =>
  R.and(dateFns.isDate, date => R.not(R.equals(date.getTime(date), NaN)))(dateFns.parse(dateStr));

const getSemesterValue = R.ifElse(R.pipe(Number, R.inc, R.gte(6)), R.always(1), R.always(2));
const applyFormat = frequency => date => {
  const hasFormat = R.includes(frequency)(R.keys(sdmxFormat));
  const format = hasFormat ? R.prop(frequency)(sdmxFormat) : R.prop(defaultFrequency)(sdmxFormat);
  return R.pipe(
    date => dateFns.format(date, format),
    R.replace(/SEMESTER/, R.pipe(dateFns.getMonth, getSemesterValue)(date)),
  )(date);
};
const dec = R.ifElse(R.gte(0), R.identity, R.dec);
const parseDate = date => dateFns.parse(date);
const addQuarters = quarter => date => dateFns.addQuarters(date, dec(quarter));
const addSemesters = (semester = 0) => date => {
  const number = dec(semester);
  if (R.lte(number)(0)) return date;
  return dateFns.addMonths(date, R.multiply(6)(dec(Number(semester))));
};
const addWeeks = (week = 1) => date => {
  const nbWeek = R.ifElse(R.equals(0), R.add(1), R.identity)(week);
  const dateToUse = R.ifElse(
    date => dateFns.isSameYear(date, dateFns.startOfISOYear(date)),
    dateFns.startOfISOYear,
    dateFns.startOfYear,
  )(dateFns.addMonths(date, 1));
  return dateFns.addWeeks(dateToUse, R.dec(nbWeek));
};

const slicerSQW = R.pipe(R.last, R.slice(1, Infinity));

const getSQW = (fn, slicer) =>
  R.map(period => {
    if (R.isEmpty(R.head(period))) return undefined;
    const year = R.head(period);
    const hasAdded = R.pipe(R.length, R.equals(2))(period) ? slicer(period) : 0;
    return R.pipe(parseDate, fn(Number(hasAdded)))(year);
  });

const parseDateFromSdmxPeriod = R.curry((frequency, sdmxPeriod = []) => {
  const periodSQW = R.map((stringPeriod = '') => R.split('-')(stringPeriod))(sdmxPeriod);
  if (R.complement(R.includes)(frequency)(['Q', 'S', 'W', 'B'])) {
    return R.map(parseDate)(sdmxPeriod);
  }
  if (R.includes(frequency)(['W', 'B'])) {
    return getSQW(addWeeks, slicerSQW)(periodSQW);
  }
  if (R.equals('Q')(frequency)) return getSQW(addQuarters, slicerSQW)(periodSQW);
  return getSQW(addSemesters, slicerSQW)(periodSQW);
});

// --------------------------------------------------------------------------------------Counter
const getDifferenceBetweenDates = (frequency, interval) => {
  const difference = R.propOr(
    dateFns.differenceInYears,
    frequency,
  )({
    A: dateFns.differenceInYears,
    S: (end, start) => dateFns.differenceInMonths(end, start) / 6,
    Q: dateFns.differenceInQuarters,
    M: dateFns.differenceInMonths,
    W: dateFns.differenceInWeeks,
    B: dateFns.differenceInWeeks,
    D: dateFns.differenceInDays,
    H: dateFns.differenceInHours,
    N: dateFns.differenceInMinutes,
  });
  return difference(R.last(interval), R.head(interval));
};

const getDateInTheRange = boundaries => date => {
  if (R.includes(undefined, boundaries)) return undefined;
  if (R.isNil(date)) return date;
  return dateFns.isWithinRange(date, ...boundaries) ? date : dateFns.closestTo(date, boundaries);
};

const addTimeList = {
  A: dateFns.addYears,
  S: (date, v) => dateFns.addMonths(date, v * 6),
  Q: dateFns.addQuarters,
  M: dateFns.addMonths,
  W: dateFns.addWeeks,
  B: dateFns.addWeeks,
  D: dateFns.addDays,
  H: dateFns.addHours,
  N: dateFns.addMinutes,
};

// -------------------------------------------------------------------------------------------------API
export const getFrequencies = R.pipe(
  R.propOr([], 'values'),
  R.map(R.props(['id', 'label'])),
  R.fromPairs,
);

export const parseInclusivesDates = (isInclusives, frequency = defaultFrequency) => dates => {
  const addTime = R.prop(frequency)(addTimeList);
  return R.addIndex(R.map)((date, index) => {
    if (R.isNil(date)) return date;
    if (R.nth(index)(isInclusives)) return date;
    if (R.equals(0, index)) return addTime(date, 1);
    return addTime(date, -1);
  })(dates);
};

export const checkDatesIsAsc = dates => {
  const startDate = R.head(dates);
  const endDate = R.last(dates);
  if (R.pipe(dateFns.compareAsc, R.flip(R.lte)(0))(startDate, endDate)) return dates;
  return [undefined, undefined];
};

// only to new date
export const changeToDates = boundaries =>
  R.pipe(
    R.map(R.pipe(dateFns.parse, dateWithoutTZ)),
    R.addIndex(R.map)((dateStr, index) =>
      R.ifElse(
        getIsValidDate,
        R.identity,
        R.always(dateFns.parse(R.nth(index)(boundaries))),
      )(dateStr),
    ),
  );

export const replaceUndefinedDates = boundaries =>
  R.pipe(
    R.addIndex(R.map)((dateStr, index) =>
      R.ifElse(
        getIsValidDate,
        R.identity,
        R.always(R.nth(index)(boundaries)),
      )(dateStr),
    ),
  ); 

export const getDateFromSdmxPeriod = R.curry((frequencyType, period) =>
  R.map(R.ifElse(getIsValidDate, R.identity, R.always(undefined)))(
    parseDateFromSdmxPeriod(frequencyType, period),
  ),
);

export const getSdmxPeriod = R.curry((frequency, date) =>
  R.ifElse(getIsValidDate, applyFormat(frequency), R.always(undefined))(date),
);

export const getIntervalPeriod = datesBoundaries => (frequency, dates = []) => {
  if (R.all(R.isNil)(dates)) return [0, getDifferenceBetweenDates(frequency, datesBoundaries) + 1];
  return [
    getDifferenceBetweenDates(frequency, replaceUndefinedDates(datesBoundaries)(dates)) + 1,
    getDifferenceBetweenDates(frequency, datesBoundaries) + 1,
  ];
}

export const changeSdmxPeriod = (frequency, boundaries) =>
  R.pipe(
    getDateFromSdmxPeriod(frequency),
    R.map(R.pipe(getDateInTheRange(boundaries), getSdmxPeriod(frequency))),
  );

export const changeFrequency = frequency =>
  R.pipe(
    R.keys,
    R.ifElse(R.includes(frequency), R.always(frequency), R.head),
    R.ifElse(R.either(R.isEmpty, R.isNil), R.always(defaultFrequency), R.identity),
  );
