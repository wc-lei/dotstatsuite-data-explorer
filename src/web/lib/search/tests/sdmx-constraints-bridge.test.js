import bs62 from 'bs62';
import md5 from 'md5';
import {
  searchConstraintsToVisConstraints,
  searchConstraintsToVisSelection,
  getDefaultSelection,
} from '../sdmx-constraints-bridge';

const dimensions = [
  {}, // empty dimension is possible, don't remember the usecase (cf dimitri)
  {
    id: 'MEASURE',
    index: 2,
    display: true,
    label: 'Measure',
    values: [
      {
        id: 'TOUR_TRIPS',
        label: 'Tourism trips',
        display: true,
        order: 0,
        parentId: undefined,
        position: 0,
      },
      {
        id: 'NIGHTS_ACCOM',
        label: 'Nights spent in tourism accommodation establishments',
        display: true,
        order: 0,
        parentId: undefined,
        position: 1,
      },
    ],
  },
  {
    id: 'REPORTING_COUNTRY',
    index: 0,
    display: true,
    label: 'Reporting country',
    values: [
      { id: 'BE', label: 'Belgium', display: true, order: 0, parentId: undefined, position: 7 },
      { id: 'BE3', label: 'Wallonia', display: true, order: 0, parentId: 'BE', position: 10 },
    ],
  },
  {
    "id": "FREQ",
    "index": 3,
    "display": true,
    "label": "Frequency",
    "roles": [],
    "values": [
      {
        "id": "A",
        "display": true,
        "label": "Annual",
        "order": 0,
        "position": 0
      },
      {
        "id": "D",
        "display": true,
        "label": "Daily",
        "order": 0,
        "position": 0
      }
    ]
  }
];

const constraints = {
  [md5(
    `${bs62.encode(
      'Measure',
    )}${'0|Nights spent in tourism accommodation establishments#NIGHTS_ACCOM#'}`,
  )]: {
    facetId: bs62.encode('Measure'),
    constraintId: '0|Nights spent in tourism accommodation establishments#NIGHTS_ACCOM#',
  },
  [md5(`${bs62.encode('Measure')}${'0|Tourism trips#TOUR_TRIPS#'}`)]: {
    facetId: bs62.encode('Measure'),
    constraintId: '0|Tourism trips#TOUR_TRIPS#',
  },
  [md5(`${bs62.encode('Reporting country')}${'1|Belgium#BE#|Wallonia#BE3#'}`)]: {
    facetId: bs62.encode('Reporting country'),
    constraintId: '1|Belgium#BE#|Wallonia#BE3#',
  },
};

const structureSelection = { REPORTING_COUNTRY: ['BE3', 'DE'], FREQ: ['A', 'B', 'C'] };

describe('sdmx constraints bridge', () => {
  it('should prepare constraints', () => {
    expect(searchConstraintsToVisConstraints(constraints)).toEqual({
      [bs62.encode('Measure')]: [
        { constraintId: 'NIGHTS_ACCOM', facetId: bs62.encode('Measure') },
        { constraintId: 'TOUR_TRIPS', facetId: bs62.encode('Measure') },
      ],
      [bs62.encode('Reporting country')]: [
        { constraintId: 'BE3', facetId: bs62.encode('Reporting country') },
      ],
    });
  });

  it('should return selection from constraints', () => {
    expect(searchConstraintsToVisSelection(dimensions, constraints)).toEqual({
      MEASURE: ['NIGHTS_ACCOM', 'TOUR_TRIPS'],
      REPORTING_COUNTRY: ['BE3'],
    });
  });

  it('should return selection from constraints with freq', () => {
    expect(getDefaultSelection(dimensions, {}, constraints)).toEqual({
      FREQ: ['A'],    
      MEASURE: ['NIGHTS_ACCOM', 'TOUR_TRIPS'],
      REPORTING_COUNTRY: ['BE3'],
    });
  });

  it('should return empty selection without frequency', () => {
    const constraints = {
      '1a5729808b5014c1cb4f3aa4a2e789e6': {
        facetId: 'datasourceId',
        constraintId: 'staging:SIS-CC-stable',
      },
    };
    expect(getDefaultSelection([{}, {}], {}, constraints)).toEqual({});
  });

  it('should return freq selection if dimensions has Freq id', () => {
    expect(getDefaultSelection(dimensions, {}, {})).toEqual({ FREQ: ['A'] });
  });

  it('should return structure selection consistency with dimensions', () => {
    expect(getDefaultSelection(dimensions, structureSelection, {})).toEqual({ REPORTING_COUNTRY: ['BE3'], FREQ: ['A'] });
  });

  it('should return selection', () => {
    const dimensions = [{ id: 'FREQ', label: 'Frequency', values: [{ id: 'A' }] }, {}];
    const constraints = {
      '1a5729808b5014c1cb4f3aa4a2e789e6': {
        facetId: bs62.encode('Frequency'),
        constraintId: 'Annual#Q#'
      },
      '1a5729808b5014c1cb4f3aa4a2e789e7': {
        facetId: bs62.encode('Frequency'), 
        constraintId: 'Annual#A#' 
      }
    };
    expect(getDefaultSelection(dimensions, structureSelection, constraints)).toEqual({
      FREQ: ['A'],
    });
    expect(getDefaultSelection(dimensions, { FREQ: ['B'] }, {})).toEqual({
      FREQ: ['A'],
    });
  });
});
