import * as R from 'ramda';
import bs62 from 'bs62';
import configParser from '../configParser';
import facetsParser from '../facetsParser';
import C from '../constants';

const mockedFacets = {
  [bs62.encode('Thème!')]: {
    type: 'tree',
    localized: true,
    buckets: [{ val: '0|Economy#ECO#', count: 9 }],
  },
  [bs62.encode('Countries')]: {
    type: 'tree',
    localized: true,
    buckets: [{ val: '0|Finland#FIN#', count: 19 }],
  },
  [C.DATASOURCE_ID]: {
    type: 'tree',
    localized: true,
    buckets: [{ val: '0|Government#GOV#', count: 29 }],
  },
};

describe('configParser', () => {
  it('no facet & no restriction', () => {
    const expected = { locale: 'en', facets: [] };
    expect(expected).toEqual(configParser()({ locale: 'en', facets: {} }));
  });

  it('facets & no restriction', () => {
    const expected = { locale: 'en', facets: facetsParser()(mockedFacets) };
    expect(expected).toEqual(configParser()({ locale: 'en', facets: mockedFacets }));
  });

  it('with facets & restriction', () => {
    const expectedFacets = R.pick([bs62.encode('Thème!')], mockedFacets);
    const expected = { locale: 'en', facets: facetsParser()(expectedFacets) };
    const restriction = { facetIds: ['Thème!'] };
    expect(expected).toEqual(configParser(restriction)({ locale: 'en', facets: mockedFacets }));
  });

  it('with facets & exception restriction (datasource)', () => {
    const expectedFacets = R.pick([C.DATASOURCE_ID], mockedFacets);
    const expected = { locale: 'en', facets: facetsParser()(expectedFacets) };
    const restriction = { facetIds: [C.DATASOURCE_ID] };
    expect(expected).toEqual(configParser(restriction)({ locale: 'en', facets: mockedFacets }));
  });
});
