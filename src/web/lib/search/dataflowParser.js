import bs62 from 'bs62';
import * as R from 'ramda';
import highlightParser from './highlightParser';
import C from './constants';

const parseHighlights = R.pipe(
  R.toPairs,
  R.map(([field, highlight]) => [
    R.ifElse(R.equals(C.DATAFLOW_ID), R.always('Id'), bs62.decode)(field),
    R.map(highlightParser, highlight),
  ]),
);

const getHightlight = key => R.pipe(R.propOr([], key), R.head);

export default options => data => {
  const { id, name, description, ...dataflow } = data;
  const highlights = R.propOr({}, id, options.highlighting);

  // if the name is long, the highlight will truncate it
  const highlightedName = R.defaultTo(R.defaultTo(id, name), getHightlight('name')(highlights));

  // if <em> is used in description, false positive
  // description is html, highlighting description may break html
  const descriptionHighlight = getHightlight('description')(highlights);
  const highlightedDescription = R.ifElse(
    R.isNil,
    R.always(description),
    R.pipe(R.replace(/<(\/)?em>/g, ''), x => R.replace(x, descriptionHighlight, description)),
  )(descriptionHighlight);

  return {
    ...dataflow,
    id,
    name: highlightedName,
    description: highlightedDescription,
    highlights: parseHighlights(R.omit(['name', 'description'], highlights)),
  };
};
