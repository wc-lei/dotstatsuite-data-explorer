import * as R from 'ramda';
import bs62 from 'bs62';
import C from './constants';
import valueTreeParser from './valueTreeParser';

const fillTree = valueParser => values => {
  const missingKeys = R.converge(R.pipe(R.difference, R.reject(R.isNil)), [
    R.pluck('parentId'),
    R.pluck('id'),
  ])(values);
  if (R.isEmpty(missingKeys)) return values;
  const missingValues = R.map(val => valueParser({ val }), missingKeys);
  return R.concat(values, fillTree(valueParser)(missingValues));
};

export default options => ([id, { /*type,*/ buckets = [] }]) => {
  const valueParser = valueTreeParser({ facetId: id, ...options });
  const values = fillTree(valueParser)(R.map(valueParser, buckets));

  return {
    id,
    values,
    count: R.pipe(R.filter(R.propEq('isSelected', true)), R.length)(values),
    label: R.ifElse(R.equals(C.DATASOURCE_ID), R.always(null), bs62.decode)(id),
    hasPath: true, // if values are hierarchical and values path are computed by the parser
  };
};
