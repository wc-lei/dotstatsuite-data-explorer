import * as R from 'ramda';
import * as L from '../';

describe('DE - lib - layout - defaultLayoutBuilder', () => {
  const _dimensions = R.fromPairs(R.times(id => [id, { id, values: [1, 2] }])(4));
  describe('defaultLayoutBuilder', () => {
    test('no special dimension, engine flow review', () => {
      const layout = { rows: [0], header: [1], sections: [2, 3] };
      expect(L.defaultLayoutBuilder(_dimensions)).toEqual(layout);
    });

    test('ref area should be pick as rows', () => {
      const dimensions = {
        ..._dimensions,
        REF_AREA: { id: 'REF_AREA', values: [1, 2] },
      };
      const layout = { rows: ['REF_AREA'], header: [0], sections: [1, 2, 3] };
      expect(L.defaultLayoutBuilder(dimensions)).toEqual(layout);
    });

    test('time should be pick as header', () => {
      const dimensions = {
        ..._dimensions,
        TIME: { id: 'TIME', values: [1, 2] },
      };
      const layout = { rows: [0], header: ['TIME'], sections: [1, 2, 3] };
      expect(L.defaultLayoutBuilder(dimensions)).toEqual(layout);
    });

    test('time and ref area reconcilation', () => {
      const dimensions = {
        ..._dimensions,
        TIME: { id: 'TIME', values: [1, 2] },
        REF_AREA: { id: 'REF_AREA', values: [1, 2] },
      };
      const layout = {
        rows: ['REF_AREA'],
        header: ['TIME'],
        sections: [0, 1, 2, 3],
      };
      expect(L.defaultLayoutBuilder(dimensions)).toEqual(layout);
    });

    test('edge case, single valid dimension', () => {
      const dimensions = { 0: { id: 0, values: [1, 2] } };
      const layout = { rows: [0], header: [], sections: [] };
      expect(L.defaultLayoutBuilder(dimensions)).toEqual(layout);
    });

    test('edge case, single valid dimension time', () => {
      const dimensions = { TIME: { id: 'TIME', values: [1, 2] } };
      const layout = { rows: ['TIME'], header: [], sections: [] };
      expect(L.defaultLayoutBuilder(dimensions)).toEqual(layout);
    });
  });

  describe('adjustment', () => {
    test('empty layout', () => {
      const layout = { rows: [], header: [], sections: [] };
      expect(L.adjustment([], layout)).toEqual(layout);
    });
    test('no adjustment is necessary, rows is not empty', () => {
      const layout = { rows: [0], header: [1], sections: [2, 3] };
      expect(L.adjustment([], layout)).toEqual(layout);
    });
    test('rows is empty, expected head() of sections', () => {
      const layout = { rows: [], header: [1], sections: [2, 3] };
      const expected = { rows: [2], header: [1], sections: [3] };
      expect(L.adjustment([], layout)).toEqual(expected);
    });
    test('rows is empty, expected head() of header', () => {
      const layout = { rows: [], header: [1], sections: [] };
      const expected = { rows: [1], header: [], sections: [] };
      expect(L.adjustment([], layout)).toEqual(expected);
    });
    test('rows is empty, and sections value 2 is not available', () => {
      const layout = { rows: [], header: [1], sections: [2, 3, 4] };
      const expected = { rows: [3], header: [1], sections: [2, 4] };
      expect(L.adjustment([2], layout)).toEqual(expected);
    });
    test('rows is empty, and header value 1 is not available', () => {
      const layout = { rows: [], header: [1, 4, 2, 3], sections: [] };
      const expected = { rows: [1], header: [4, 2, 3], sections: [] };
      expect(L.adjustment([2], layout)).toEqual(expected);
    });
  });

  describe('isInvalid', () => {
    test('layout is valid, all ids are in dimensionIds and layout is not empty', () => {
      const dimensionIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
      const layoutIds = [0, 1, 2, 3];
      const layout = { rows: [0], header: [1], sections: [2, 3] };
      expect(L.isInvalid(dimensionIds, layoutIds, layout)).toEqual(false);
    });
    test('layout is empty, not valid', () => {
      const dimensionIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
      const layoutIds = [0, 2, 3];
      const layout = {};
      expect(L.isInvalid(dimensionIds, layoutIds, layout)).toEqual(true);
    });
    test('layoutIds has one different id that dimensionIds  ', () => {
      const dimensionIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
      const layoutIds = [0, 2, 3, 50];
      const layout = { rows: [0], header: [1], sections: [2, 3] };
      expect(L.isInvalid(dimensionIds, layoutIds, layout)).toEqual(true);
    });
    test('layout is empty, not valid', () => {
      const dimensionIds = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
      const layoutIds = [0, 2, 3];
      const layout = { rows: [], header: [], sections: [] };
      expect(L.isInvalid(dimensionIds, layoutIds, layout)).toEqual(true);
    });
  });

  describe('manyFormat', () => {
    test('return format take by DETableConfig', () => {
      const many = { test: { id: 'test', values: [{ id: 'testValues' }] } };
      const expected = {
        test: {
          id: 'test',
          isTimePeriod: false,
          name: undefined,
          value: 'test',
          values: [{ id: 'test-0', label: 'Xxxx' }],
        },
      };
      expect(L.manyFormat(many)).toEqual(expected);
    });
  });

  describe('oneFormat', () => {
    test('return format take by DETableConfig', () => {
      const one = { test: { id: 'test' } };
      const expected = {
        test: {
          id: 'test',
          value: 'test',
          isHidden: true,
        },
      };
      expect(L.oneFormat(one)).toEqual(expected);
    });
  });

  describe('getDefaultLayout', () => {
    test('many missings value 4, add in sections', () => {
      const getMissingIds = R.difference([4]);
      const layout = {
        rows: [0],
        sections: [2, 3, 4],
        header: [1],
      };
      expect(L.getDefaultLayout(_dimensions, getMissingIds)).toEqual(layout);
    });
  });

  describe('getLayout', () => {
    test('correct layout ouput', () => {
      const oneIds = [3];
      const missingIds = [4];
      const layout = { rows: [], sections: [1, 2], header: [] };

      expect(L.getLayout(missingIds, oneIds)(layout)).toEqual({
        rows: [1],
        sections: [2, 4],
        header: [],
      });
      expect(L.getLayout([], [])(layout)).toEqual({ rows: [1], sections: [2], header: [] });
    });
  });
});
