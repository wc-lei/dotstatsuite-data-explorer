import { theme, viewer } from '../settings';

const fonts = { fontFamily: theme.visFont };
export const options = {
  ...viewer,
  fonts: {
    chart: {
      axisLegend: fonts,
      chartLegend: fonts,
      tooltip: { primary: fonts, secondary: fonts },
    },
    footer: { copyright: fonts, source: fonts },
  },
};
