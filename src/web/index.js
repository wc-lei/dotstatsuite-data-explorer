import '@babel/polyfill';
import { Provider } from 'react-redux';
import { KeycloakProvider } from 'react-keycloak';
import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'connected-react-router';
import * as R from 'ramda';
import CssBaseline from '@material-ui/core/CssBaseline';
import configureStore, { history } from './configureStore';
import configureKeycloak from './configureKeycloak';
import { I18nProvider, initialize as initializeI18n } from './i18n';
import { initialize as initializeAnalytics } from './utils/analytics';
import { ThemeProvider } from './theme';
import searchApi from './api/search';
import * as Settings from './lib/settings';
import ErrorBoundary from './components/error';
import Helmet from './components/helmet';
import App from './components/App';
import Vis from './components/vis';
import Search from './components/search';
import meta from '../../package.json';

console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console

const initialState = {};
const gaToken = R.path(['CONFIG', 'gaToken'], window);
const store = configureStore(initialState, { gaToken });
const keycloakProps = configureKeycloak(store);

searchApi.setConfig(Settings.search);
initializeAnalytics({ token: gaToken });
initializeI18n(Settings.i18n);

const render = () => {
  ReactDOM.render(
    <KeycloakProvider {...keycloakProps}>
      <ErrorBoundary>
        <Provider store={store}>
          <ThemeProvider theme={Settings.theme}>
            <I18nProvider messages={window.I18N}>
              <ErrorBoundary>
                <Helmet />
                <CssBaseline />
                <ConnectedRouter history={history}>
                  <App>
                    <Switch>
                      <Route exact path="/" component={Search} />
                      <Route path="/vis" component={Vis} />
                      <Route component={() => <div>Not Found</div>} />
                    </Switch>
                  </App>
                </ConnectedRouter>
              </ErrorBoundary>
            </I18nProvider>
          </ThemeProvider>
        </Provider>
      </ErrorBoundary>
    </KeycloakProvider>,
    document.getElementById('root'),
  );
};

render();
