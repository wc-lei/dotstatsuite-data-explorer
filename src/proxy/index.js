import express from 'express';
import proxy from 'http-proxy-middleware';
import debug from 'debug';
import logger from 'morgan-debug';
import webpack from 'webpack';
import config from '../../config/webpack.config.dev';
import createDevServerConfig from '../../config/webpackDevServer.config';
import { createCompiler, prepareUrls } from 'react-dev-utils/WebpackDevServerUtils';
import WebpackDevServer from 'webpack-dev-server';
import initConfig from '../server/init/config';
const loginfo = debug('webapp:proxy');

const appName = 'proxy';
const useYarn = true;
// const getUrl = server => `http://${server.address().address}:${server.address().port}`;

const filterDevServer = (pathname, req) => {
  if (req.url.match('^/static')) return true;
  if (req.url.match(/sockjs-node/)) return true;
  if (req.url.match(/hot-update/)) return true;
  return false;
};

const filterConfig = (pathname, req) => {
  if (req.url.match('^/assets')) return true;
  return false;
};

const filterApi = () => true;

const runServer = (port, host, devServer) =>
  new Promise((resolve, reject) => {
    devServer.listen(port, host, err => {
      if (err) return reject(err);
      return resolve();
    });
  });

const initProxy = ctx => {
  const {
    config: { configUrl, isProduction, server: apiSrv, proxy: proxySrv },
  } = ctx;
  const app = express();
  const urls = prepareUrls('http', proxySrv.host, proxySrv.port + 1);
  const compiler = createCompiler(webpack, config, appName, urls, useYarn);
  const serverConfig = createDevServerConfig({}, urls.lanUrlForConfig);
  const devServer = new WebpackDevServer(compiler, serverConfig);

  // Route to config server
  const configOptions = { target: configUrl };
  const proxyConfig = proxy(filterConfig, configOptions);
  app.use(proxyConfig);

  // Route to webpackDevServer
  if (!isProduction) {
    const devServerOptions = {
      target: `http://${proxySrv.host}:${proxySrv.port + 1}`,
      ws: true,
    };
    const proxyDevServer = proxy(filterDevServer, devServerOptions);
    app.use(proxyDevServer);
  }

  // Route to API nodeJS server
  const apiOptions = { target: `http://${apiSrv.host}:${apiSrv.port}` };
  const proxyApi = proxy(filterApi, apiOptions);
  app.use(proxyApi);

  app.use(logger('webapp:proxy', 'dev'));
  return runServer(proxySrv.port + 1, proxySrv.host, devServer).then(() =>
    runServer(proxySrv.port, proxySrv.host, app),
  );
};

initConfig()
  .then(initProxy)
  .then(() => loginfo('started'))
  .catch(console.error); // eslint-disable-line no-console
