require('@babel/polyfill');
import debug from '../debug';

const axios = require('axios');
const { prop, merge } = require('ramda');
const { HTTPError } = require('../utils/errors');

export const getResourceUri = (uri, path, base = '/configs') => `${uri}${base}${path}`;

export const getI18nUri = (tenantId, appId, lang) => `/${tenantId}/${appId}/i18n/${lang}.json`;
export const getSettingsUri = (tenantId, appId) => `/${tenantId}/${appId}/settings.json`;
export const getDataSourcesUri = () => `/datasources.json`;
export const getTenantsUri = () => '/tenants.json';

const getResource = ({ /*appId,*/ configUrl }, options = {}) => path =>
  axios
    .get(getResourceUri(configUrl, path, options.base))
    .then(({ data }) => data)
    .catch(err => {
      if (err.response && err.response.status === 404 && 'notFoundValue' in options)
        return options.notFoundValue;
      debug.error(err);
      throw new HTTPError(err.response.status);
    });

const getI18n = config => ({ id }, langs = []) =>
  Promise.all(
    langs.map(lang =>
      Promise.all([
        getResource(config, { notFoundValue: {}, base: '/i18n' })(`/${lang}.json`),
        getResource(config, { notFoundValue: {} })(getI18nUri(id, config.appId, lang)),
      ]).then(([base, overrides]) => merge(base, overrides)),
    ),
  );

const getTenants = config => () => getResource(config)(getTenantsUri());
const getTenant = config => async id => {
  const tenant = prop(id, await getTenants(config)());
  if (!tenant) throw new HTTPError(404, `Unknown tenant '${id}' from config server`);
  return tenant;
};

const getSettings = config => ({ id }) =>
  getResource(config, { notFoundValue: {} })(getSettingsUri(id, config.appId));

const getDataSources = config => ({ id }) =>
  getResource(config, { notFoundValue: {} })(getDataSourcesUri(id, config.appId));

const provider = config => ({
  getI18n: getI18n(config),
  getTenant: getTenant(config),
  getTenants: getTenants(config),
  getSettings: getSettings(config),
  getDataSources: getDataSources(config),
  getResource: getResource(config),
});

export default provider;
