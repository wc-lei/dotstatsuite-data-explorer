import debug from '../../debug';

const { HTTPError } = require('../../utils/errors');

async function doHealthcheck() {
  const { startTime, gitHash, configProvider } = this.globals;
  const {
    req: { tenant },
  } = this.locals;
  try {
    const tenants = await configProvider.getTenants();
    return {
      tenant: tenant.id,
      gitHash,
      status: 'OK',
      configProvider: {
        status: 'OK',
        partners: Object.values(tenants).length,
      },
      startTime,
    };
  } catch (err) {
    debug.error(err);
    throw new HTTPError(500, 'ConfigServer is unreachable');
  }
}

const healthcheck = {
  name: 'healthcheck',
  get: doHealthcheck,
};

const init = evtx => evtx.use(healthcheck.name, healthcheck);

module.exports = init;
