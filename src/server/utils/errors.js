function HTTPError(code, message) {
  Error.captureStackTrace(this, HTTPError);
  this.message = message || `HTTP Error code ${code}`;
  this.name = 'HTTPError';
  this.code = code;
}

HTTPError.prototype = Object.create(Error.prototype);
HTTPError.prototype.constructor = HTTPError;

function ClientNotFoundError(id) {
  Error.captureStackTrace(this, ClientNotFoundError);
  this.message = `Client#${id} not found`;
  this.name = 'ClientNotFoundError';
  this.id = id;
}

ClientNotFoundError.prototype = Object.create(Error.prototype);
ClientNotFoundError.prototype.constructor = ClientNotFoundError;

module.exports = { ClientNotFoundError, HTTPError };
